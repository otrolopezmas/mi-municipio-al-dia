<?php

/**
 * Mi Municipio al Día
 * 
 * @author Marcos Lopez
 * 
 * Configuration file where routes are declared
 */
require_once 'core/router.php';
$router = new Router();

$router->post('/login/', array(
        'controller' => 'login',
        'action' => 'login'
        )
);

//Select all fiestas

$router->get('/fiestas/', array(
        'controller' => 'fiestas',
        'action' => 'get_fiestas'
        )
);

//Select specific fiesta

$router->get('/fiestas/id', array(
        'controller' => 'fiestas',
        'action' => 'get_fiesta'
        ), array('id' => '\d+'));

//General insert for fiestas

$router->post('/fiestas/', array(
        'controller' => 'fiestas',
        'action' => 'insert_fiesta'
        )
);

//Update fiesta

$router->put('/fiestas/id', array(
        'controller' => 'fiestas',
        'action' => 'edit_fiesta'
        ), array('id' => '\d+'));

//Delete fiesta

$router->delete('/fiestas/id', array(
        'controller' => 'fiestas',
        'action' => 'delete_fiesta'
        ), array('id' => '\d+'));


$router->get('/lugares/', array(
        'controller' => 'lugares',
        'action' => 'get_lugares'
));

$router->post('/lugares/', array(
        'controller' => 'lugares',
        'action' => 'add_lugar'
));

$router->delete('/lugares/coords', array(
        'controller' => 'lugares',
        'action' => 'delete_lugar'
        ), array('coords' => '\d+.\d+x-\d+.\d+'));


//Retrieve all the events from a certain fiesta

$router->get('/fiestas/id/eventos_votaciones/', array(
        'controller' => 'eventos',
        'action' => 'get_eventos_votaciones'
        ), array('id' => '\d+'));

$router->get('/fiestas/id/export', array(
        'controller' => 'eventos',
        'action' => 'export_eventos'
        ), array('id' => '\d+'));

$router->post('/fiestas/id/eventos/', array(
        'controller' => 'eventos',
        'action' => 'insert_evento'
        ), array('id' => '\d+'));

$router->put('/fiestas/id/eventos/id', array(
        'controller' => 'eventos',
        'action' => 'edit_evento'
        ), array(
        'id' => '\d+',
        'id_evento' => '\d+'
));

$router->delete('/fiestas/id/eventos/id', array(
        'controller' => 'eventos',
        'action' => 'delete_evento'
        ), array(
        'id' => '\d+',
        'id_evento' => '\d+'
));


