<?php

/**
 * Mi Municipio al Dia
 * 
 * @author Marcos Lopez
 * 
 * Database class
 * 
 * Contains the neccesary methods for basic CRUD operations
 */
class DB {

        private $conexion;
        private $host;
        private $user;
        private $pass;
        private $nombre_base;

        /**
         * Initialize the object with the database connection information
         */
        public function __construct() {
                $this->host = DB_SERVER;
                $this->user = DB_USER;
                $this->pass = DB_PASS;
                $this->nombre_base = DB_NAME;
        }

        /**
         * Establish a connection to database
         */
        public function connect() {
                try {
                        $this->conexion = new PDO("mysql:host=$this->host;dbname=$this->nombre_base;charset=UTF8", $this->user, $this->pass);
                        $this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                } catch (PDOException $ex) {
                        $r = new Response(500, 'Error con la conexión a la base de datos', $ex->getMessage());
                }
        }

        /**
         * Close active database connection
         */
        public function close() {
                $this->conexion = null;
        }

        /**
         * Ejecuta una sentencia de tipo SELECT y devuelve un array asociativo
         * con los resultados obtenidos o un array de arrays asociativos.
         * 
         * @param String $sql
         * @param associative array $datos
         * 
         * @return array
         */
        public function select($sql, $datos = NULL) {
                
                try {
                        $preparado = $this->conexion->prepare($sql);

                        if (!is_null($datos)) {
                                foreach ($datos as $indice => &$valor) {
                                        $preparado->bindParam($indice, $valor);
                                }
                        }

                        $preparado->execute();

                        if ($preparado->rowCount() == 1) {
                                $resultado = $preparado->fetch(PDO::FETCH_ASSOC);
                        } else {
                                $resultado = array();
                                while ($fila = $preparado->fetch(PDO::FETCH_ASSOC)) {
                                        $resultado[] = $fila;
                                }
                        }

                        $preparado->closeCursor();
                        
                        return $resultado;
                        
                } catch (PDOException $ex) {
                        $msg = 'Se ha encontrado un error al interactuar con la base de datos. Si el error persiste' .
                                ' por favor contacte con los responsables';
                        $r = new Response(500, null, $msg, $ex->getMessage());
                }
        }

        /**
         * Execute a non type SELECT consult to database
         * 
         * @param String $sql
         * @param array $datos
         * 
         * @return array $result
         * 
         */
        public function no_select($sql, $datos = NULL) {
                
                try {
                        $preparado = $this->conexion->prepare($sql);

                        if (!is_null($datos)) {
                                foreach ($datos as $indice => &$valor) {
                                        $preparado->bindParam($indice, $valor);
                                }
                        }
                        $result = $preparado->execute();
                        $preparado->closeCursor();

                        return $result;
                        
                } catch (PDOException $ex) {
                        
                        $msg = 'Se ha encontrado un error al interactuar con la base de datos. Si el error persiste' .
                                ' por favor contacte con los responsables';
                        $r = new Response(500, null, $msg, $ex->getMessage());
                }
        }

}
