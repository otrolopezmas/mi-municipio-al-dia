-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 173.194.86.172    Database: dfef
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Evento`
--

DROP TABLE IF EXISTS `Evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Evento` (
  `idEvento` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Descripcion` varchar(1000) DEFAULT NULL,
  `Tipo` int(11) NOT NULL,
  `Dia` date NOT NULL,
  `HoraInicio` varchar(4) NOT NULL,
  `HoraFin` varchar(4) DEFAULT NULL,
  `VotacionLocal` double DEFAULT NULL,
  `numVotosLocal` int(11) DEFAULT NULL,
  `VotacionVisitante` double DEFAULT NULL,
  `numVotosVisitante` int(11) DEFAULT NULL,
  `esNoticia` tinyint(1) DEFAULT NULL,
  `idFiesta` int(11) NOT NULL,
  `Latitud` double DEFAULT NULL,
  `Longitud` double DEFAULT NULL,
  PRIMARY KEY (`idEvento`),
  KEY `fk_Evento_Fiesta1_idx` (`idFiesta`),
  KEY `fk_Evento_Lugar1_idx` (`Latitud`,`Longitud`),
  CONSTRAINT `fk_Evento_Fiesta1` FOREIGN KEY (`idFiesta`) REFERENCES `Fiesta` (`idFiesta`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Evento_Lugar1` FOREIGN KEY (`Latitud`, `Longitud`) REFERENCES `Lugar` (`Latitud`, `Longitud`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=908 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Evento`
--

LOCK TABLES `Evento` WRITE;
/*!40000 ALTER TABLE `Evento` DISABLE KEYS */;
INSERT INTO `Evento` VALUES (153,'Concentracion musical','Charanga para ir al pregon',2,'2015-08-24','2130','',7.5,4,0,0,1,25,41.7082398,-1.000588),(154,'Chupinazo ','peña El Barbo',7,'2015-08-24','2000','',0,0,0,0,1,25,41.711934,-0.99766),(156,'Concurso de Carrozas y comparsas','',3,'2015-08-24','2035','',9,1,0,0,1,25,41.7114982,-0.9972644),(157,'Desfile con Batucada','',7,'2015-08-24','2120','',7,2,0,0,1,25,41.7204981,-1.0253394),(158,'Revista Espectaculo','',7,'2015-08-24','2240','',0,0,0,0,1,25,41.7052505,-0.9986491),(159,'Disco movil','Show Pin-Pan',2,'2015-08-24','2330','',5,1,0,0,1,25,41.7130425,-0.9967132),(160,'Concurso','Tiro con Carabina, mayores de 18',3,'2015-08-25','1130','',0,0,0,0,1,25,41.7087557,-0.9986465),(161,'Encierro','Ganaderia Hnos Matlin',1,'2015-08-25','1235','',0,0,0,0,1,25,41.7087557,-0.9986465),(162,'Torneo de Rabino','Incripcion 1/2 horas antes',3,'2015-08-25','1645','',5,1,0,0,1,25,41.7087557,-0.9986465),(163,'Encierro','Ganaderia Martin',1,'2015-08-25','1800','',0,0,0,0,1,25,41.7067257,-0.998605),(164,'Comparsa y Cabezudos','Amenizada por Charanga los Santos Alicates',4,'2015-08-25','2015','',0,0,0,0,1,25,41.7067257,-0.998605),(165,'Animacion de calle','Con Pepin a la Americana',4,'2015-08-25','2030','',0,0,0,0,1,25,41.7082398,-1.000588),(166,'Concierto','Los Toyos, musica Folk',2,'2015-08-25','2230','',0,0,0,0,1,25,41.711934,-0.99766),(167,'Fiesta','Tematica Ibiza',2,'2015-08-25','2115','',0,0,0,0,1,25,41.7114982,-0.9972644),(168,'Torneo Futbolin','Inscripción 1/2 h antes',3,'2015-08-25','1645','',0,0,0,0,1,25,41.7067257,-0.998605),(169,'Exhibicion de sevillanas','Inscripción 1/2 h antes',7,'2015-08-25','1705','',0,0,0,0,1,25,41.7067257,-0.998605),(170,'Encierro','Ganaderia Martín',1,'2015-08-26','1200','',0,0,0,0,1,25,41.7067257,-0.998605),(172,'Encierro','Ganaderia Martín',1,'2015-08-26','1700','',7,1,0,0,1,25,41.7067257,-0.998605),(176,'Torneo Futbolin','Inscripción 1/2 h antes',3,'2015-08-26','1650','',0,0,0,0,1,25,41.7067257,-0.998605),(177,'Concurso Pelotapon','Organizado por Zarandeo',3,'2015-08-26','1900','',0,0,0,0,1,25,41.7067257,-0.998605),(178,'Chiqui Encierro','',4,'2015-08-26','2015','',6,1,0,0,1,25,41.7067257,-0.998605),(179,'Teatro Infantil de Calle','',4,'2015-08-26','2030','',0,0,0,0,1,25,41.7063095,-0.9984257),(180,'Verbena','Orquesta Welcome Band',2,'2015-08-26','2355','',0,0,0,0,1,25,41.7130425,-0.9967132),(181,'Discomovil','Interplaza principal',2,'2015-08-26','2355','',0,0,0,0,1,25,41.7130425,-0.9967132),(182,'Animacion Pre-chupinazo ','El Buda',4,'2015-08-27','1605','',5,1,0,0,1,25,41.7067257,-0.998605),(183,'Concentracion musical','Charanga para ir al pregon',2,'2015-08-27','2000','',7.2,5,0,0,1,25,41.7082398,-1.000588),(184,'Chupinazo ','peña El Barbo',7,'2015-08-27','1900','',6.5,2,0,0,1,25,41.711934,-0.99766),(185,'Encierro','Ganaderia Martin',1,'2015-08-27','1800','',6.666666666666667,3,0,0,1,25,41.7067257,-0.998605),(186,'Encierro','Ganaderia Hnos Matlin',1,'2015-08-27','1205','',7,1,0,0,1,25,41.7087557,-0.9986465),(187,'Fiesta','Tematica Ibiza',2,'2015-08-27','2230','',6.5,2,0,0,1,25,41.7114982,-0.9972644),(188,'Animacion de calle','Con Pepin a la Americana',4,'2015-08-27','2030','',9,1,0,0,1,25,41.7082398,-1.000588),(440,'Trofeo \"Joaquín Plumed\"','Nuestro equipo el C.D.Monreal se medirá al equipo Alcorisa C.D. Colabora: \"Peña Los Bohemios\".',3,'2015-08-15','1830','',7.615384615384615,13,0,0,1,41,40.7877194,-1.3553635),(441,'Final del torneo del Club de Petanca','Final del torneo del Club de Petanca en las pistas del \"pradico de la fuente\".',3,'2015-08-20','1730','',6,2,0,0,1,43,40.7877194,-1.3553635),(442,'Festifalk','Conjunto Folklórico \"Tarlac State University\" de Filipinas (Entrada 10 euros o con bono)',2,'2015-08-20','2230','',5,1,0,0,1,43,40.7877194,-1.3553635),(443,'Gran Carrera de Pollos','Un año más comenzamos las fiestas a toda prisa con la ¡Gran Carrera de Pollos! Desde la revuelta de los ojos hasta casa Kiko',3,'2015-08-21','1830','',5.5,2,0,0,1,43,40.7877194,-1.3553635),(444,'Presentación Oficial','Tras una ducha y con nuestras mejores galas acompañaremos a nuestra Corte de Honor, desde la Plaza Mayor hasta el Pabellón.',7,'2015-08-21','2000','',3,2,0,0,1,43,40.7903605,-1.3532413),(445,'Toro embolado ','Las quintas quieren encender las bolas del Toro embolado ¿se atreverán? También habrá novillas para el público más atrevido y todo animado por Charanga Los Rabudas. (Entrada o bono de toros).',1,'2015-08-21','0030','',2,1,0,0,1,43,40.7877194,-1.3553635),(446,'Concurso de talentos e improvisaciones artísticas','¡Anda-Anda! Concurso de talentos e improvisaciones artísticas, con premios. En peña La Kokotxa.',3,'2015-08-21','0030','',10,1,0,0,1,43,40.7877194,-1.3553635),(447,'Makro Disco-móvil Sancho´s','Hará que bailes hasta el amanecer... o hasta que vengan a limpiar el pabellón y os echen a escobazos (entrada gratuita).',2,'2015-08-21','0130','',8,2,0,0,1,43,40.7877194,-1.3553635),(448,'¡ESTAMOS EN FIESTAS!','Ya suenan los cohetes. ¡ESTAMOS EN FIESTAS!',7,'2015-08-22','0900','',0,0,0,0,1,43,40.7877194,-1.3553635),(449,'Cabezudos ','Primer día de Cabezudos acompañados de la charanga Los Espontáneos.',4,'2015-08-22','1200','',0,0,0,0,1,43,40.7877194,-1.3553635),(450,'Fiesta Holi \"Guerra de Colores\"','Compra tu bolsa al sacar el bono y ¡no te traigas tus mejores galas!.',7,'2015-08-22','1730','',0,0,0,0,1,43,40.7877194,-1.3553635),(451,'Chupinazo','Palabricas de la comisión, del alcalde, Chupinazo, fiesta de la espuma y recorrido de la charanga por las peñas ¿Alguien da más?',7,'2015-08-22','1800','',0,0,0,0,1,43,40.7877194,-1.3553635),(452,'Disco-móvil Sancho´s ','Baile popular, para todos los públicos.',2,'2015-08-22','2000','',0,0,0,0,1,43,40.7905362,-1.3529612),(453,'Verbena Welcome Band','Que mejor nombre podría tener una orquesta para dar la Bienvenida esta noche a todos los que nos visitan. Verbena con la Orquesta Welcome Band (Entrada 5 euros o con el Bono).',2,'2015-08-22','0000','',0,0,0,0,1,43,40.7877194,-1.3553635),(454,'Disco-móvil Sancho´s','Que si que ya sabemos que tenéis ganas de fiesta, pues hala seguir con Disco-móvil Sancho´s.',2,'2015-08-22','0445','',10,1,0,0,1,43,40.7877194,-1.3553635),(457,'Misa Baturra y Procesión','¡¡Ojo con la hora que este año por razones de agenda pastoral se adelanta!! Misa Baturra y Procesión, en honor a la Natividad de Nuestra Señora, a cargo de la Asociación Jotera \"La rosa del Azafrán\".',6,'2015-08-23','1100','',0,0,0,0,1,43,40.7877194,-1.3553635),(458,'Cabezudos  ','Y por eso, los Cabezudos  madrugarán un poco menos, veis al final bien para todos.',4,'2015-08-23','1230','',9,1,0,0,1,43,40.7877194,-1.3553635),(459,'Orquesta La Fania','Primer Café concierto de Fiestas con la Orquesta La Fania. (Entrada 3 euros o con bono).',2,'2015-08-23','1700','',0,0,0,0,1,43,40.7877194,-1.3553635),(460,'Gran Concurso de Recortes','Rápido a la peña a coger la nevera y acudir todos al Gran Concurso de Recortes y suelta de Novillas para el público (Bono toros).',1,'2015-08-23','1830','',5,1,0,0,1,43,40.7877194,-1.3553635),(461,'Disco-móvil Sancho´s','Baile popular con Disco-móvil Sancho´s en la Calle Mayor.',2,'2015-08-23','2000','',0,0,0,0,1,43,40.7905362,-1.3529612),(463,'Tiro de pita de Oliva Aragonesa','La Viga y La Chimenea organizan el IV Campeonato Regional infantil de \"Tiro de pita de Oliva Aragonesa\" en la calle de la peña La Viga. ',3,'2015-08-23','2030','',0,0,0,0,1,43,40.7877194,-1.3553635),(464,'Concurso de play-back','A ver cómo van esas coreografías del Concurso de play-back infantil en el Patio de las Beltranas.',3,'2015-08-23','2030','',10,1,0,0,1,43,40.7903605,-1.3532413),(465,'Carretones','Y en el mismo sitio nuestros pequeños podrán disfrutar ¡de nuestros Carretones!',4,'2015-08-23','2100','',0,0,0,0,1,43,40.7877194,-1.3553635),(466,'Fiesta Guateque ','El grupo \"Ponme la penúltima\" hará una Fiesta Guateque en la Peña Las Anónimas, ¡ven ambientao!. Habrá ponche, cerveza, tequila, música y ... sorpresas.',2,'2015-08-23','2300','',0,0,0,0,1,43,40.7877194,-1.3553635),(467,'Verbena La Fania ','Gran Verbena con la Orquesta La Fania (Entrada 5 euros o con el Bono).',2,'2015-08-23','0045','',10,1,0,0,1,43,40.7877194,-1.3553635),(469,'Juegos tradicionales','juegos tradicionales.',4,'2015-08-24','1130','',0,0,0,0,1,43,40.7905362,-1.3529612),(470,'Cabezudos','A que no me pillas, cara de... ¡Los Cabezudos están aquí!',4,'2015-08-24','1200','',0,0,0,0,1,43,40.7877194,-1.3553635),(471,'Concurso de guiñote ','1ª Fase del Concurso de guiñote en el Pabellón.',3,'2015-08-24','1530','',0,0,0,0,1,43,40.7899459,-1.3488493),(472,'Orquesta La Mundial','Café concierto con la Orquesta La Mundial. (Entrada 3 euros o con bono).',2,'2015-08-24','1700','',0,0,0,0,1,43,40.7877194,-1.3553635),(473,'Novillada','Exhibición de doma de circo (si no sabes lo que es, pues vienes a verlo y así no tienes que preguntar luego). Novillada sin muerte y para el público vaquillas. (Bono de toros).',1,'2015-08-24','1830','',6,1,0,0,1,43,40.7877194,-1.3553635),(474,'Disco-móvil Sancho´s','Baile popular con Disco-móvil Sancho´s en la Calle Mayor.',2,'2015-08-24','2000','',0,0,0,0,1,43,40.7905362,-1.3529612),(475,'Jota ','Espectáculo de Jota \"Pueblo de ayer\" del Grupo D’Aragon, en el pabellón. (Entrada 10 euros o con bono).',2,'2015-08-24','2300','',0,0,0,0,1,43,40.7899459,-1.3488493),(476,'Verbena con la Orquesta La Mundial','Noche temática, este año: Habia una vez...\"El Circo(Entrada 5 euros o con bono).',2,'2015-08-24','0045','',0,0,0,0,1,43,40.7877194,-1.3553635),(477,'Juegos','Hoy ven a jugar a la pinta y el tanganillo.(bono).',4,'2015-08-25','1130','',0,0,0,0,1,43,40.7905362,-1.3529612),(478,'Cabezudos','Si todavía te quedan fuerzas ven a correr a los Cabezudos.',4,'2015-08-25','1200','',0,0,0,0,1,43,40.7877194,-1.3553635),(479,'Concurso de guiñote ','Fase final del Concurso de guiñote en el Pabellón.',3,'2015-08-25','1600','',0,0,0,0,1,43,40.7899459,-1.3488493),(480,'Orquesta Nueva Alaska','Últimas pastas, última orquesta y último Café Concierto con la Orquesta Nueva Alaska (Entrada 3 euros o con bono).',2,'2015-08-25','1700','',0,0,0,0,1,43,40.7877194,-1.3553635),(481,'Fiesta de la cerveza ','Fiesta de la cerveza en el frontón viejo, organizado por las peñas Tol Perkal, El Desbarajuste y con la colaboración de los Pifias Mentales y nuestra charanga local \"Los Rabudas\".',7,'2015-08-25','1700','',10,1,0,0,1,43,40.7877194,-1.3553635),(482,'Concurso de Bolos','Concurso de Bolos en el recinto del pabellón (bono).(Servicio Comarcal Deportes).',3,'2015-08-25','1700','',0,0,0,0,1,43,40.7899459,-1.3488493),(483,'Partido de Pelota Mano','Nuestros amigos de Garfe Pelotaris nos ofrecerán un Partido de Pelota Mano en el Frontón.',3,'2015-08-25','1830','',10,1,0,0,1,43,40.7877194,-1.3553635),(484,'Partido de fútbol','Partido de fútbol entre solteros y casados o lo que se tercie, a ver como barajamos los equipos.',3,'2015-08-25','1900','',0,0,0,0,1,43,40.7877194,-1.3553635),(486,'Concurso de Disfraces ','Mientras los peques la gozarán en el Concurso de Disfraces infantil en el Patio de las Beltranas.',3,'2015-08-25','1930','',0,0,0,0,1,43,40.7903605,-1.3532413),(487,'Animación infantil','Y después todos a bailar en la Animación infantil con \"Menuda Banda\" de Almozandia en el Patio de las Beltranas.',4,'2015-08-25','2030','',0,0,0,0,1,43,40.7903605,-1.3532413),(488,'Espectáculo de Humor','30 Este año cambiamos, esperamos que os guste el Espectáculo de Humor: \"ENRADIADOS\" con Javier Segarra y Mariano-Mariano. (Entrada 10 euros o con bono).\r\nQueremos dar un agradecimiento especial a Pyrsa por colaborar económicamente con la contratación de este espectáculo y celebrar así, junto a nosotros, su 25 aniversario.\r\n¡Felicidades Pyrsa y que cumplas muchos más!',7,'2015-08-25','2230','',8,1,0,0,1,43,40.7877194,-1.3553635),(489,'Orquesta Nueva Alaska','Estupenda Verbena con la fabulosa Orquesta Nueva Alaska . (Entrada 5 euros o bono).',2,'2015-08-25','0045','',0,0,0,0,1,43,40.7877194,-1.3553635),(490,'Actuaciones libres','Actuaciones libres por peñas, o juegos, o algo haremos, venir preparados para todo.',7,'2015-08-25','0300','',0,0,0,0,1,43,40.7877194,-1.3553635),(491,'Charanga','Todos a la puerta que se va la Charanga.',2,'2015-08-25','0530','',0,0,0,0,1,43,40.7877194,-1.3553635),(492,'Tiro de Barra Aragonesa, Calva y Tirachinas','',4,'2015-08-26','1130','',0,0,0,0,1,43,40.7899459,-1.3488493),(493,'Cabezudos','Los cabezudos se despiden de todos nosotros hasta el año que viene.',4,'2015-08-26','1200','',5.5,2,0,0,1,43,40.7877194,-1.3553635),(494,'Parque infantil','Los cabezudos se despiden de todos nosotros hasta el año que viene.',4,'2015-08-26','1300','1400',0,0,0,0,1,43,40.7905362,-1.3529612),(495,'Toro ensogado','Toro ensogado por las calles del pueblo a cargo de  Asociación de Amigos de la Soga y la Baga de Teruel. Queda totalmente prohibido para los menores y personas con movilidad reducida, estar dentro del recorrido. Se ruega máxima precaución.',1,'2015-08-26','1600','',0,0,0,0,1,43,40.7877194,-1.3553635),(496,'Parque infantil','',4,'2015-08-26','1600','2000',0,0,0,0,1,43,40.7905362,-1.3529612),(497,'Toreros de Monreal','Ven a animar a nuestros jóvenes en el Especial toreros de Monreal (Entrada o bono toros).',1,'2015-08-26','1830','',0,0,0,0,1,43,40.7877194,-1.3553635),(499,'Concurso de Autos Locos','Cambiamos el recorrido, pero no las ganas, ni la velocidad del Concurso de Autos Locos. Está organizado por A.C. \"Circuito La Hoz\".',3,'2015-08-26','2030','',0,0,0,0,1,43,40.7903605,-1.3532413),(500,'Disco-móvil Sancho´s','Últimos bailes con Disco-móvil Sancho´s.',2,'2015-08-26','2000','2230',0,0,0,0,1,43,40.7905362,-1.3529612),(501,'Fuegos Artificiales','Con los Fuegos Artificiales de Pirotécnia Tomás despediremos nuestras fiestas.',7,'2015-08-26','0030','',0,0,0,0,1,43,40.7877194,-1.3553635),(502,'Disco Móvil Sancho´s','Y como estos chicos se multiplican Disco Móvil Sancho´s continuará en el pabellón con música para todos los públicos. (Entrada 3 euros o bono).',2,'2015-08-26','0045','',0,0,0,0,1,43,40.7899459,-1.3488493),(507,'Charanga ','Charanga Los Espontáneos nos llevará de peña en peña hasta \"La Escapada\" que nos dará de almorzar.',2,'2015-08-24','0530','',10,1,0,0,1,43,40.7877194,-1.3553635),(512,'Charanga Los Espontáneos','Charanga Los Espontáneos nos lleva de ruta por el pueblo.',2,'2015-08-23','0700','',0,0,0,0,1,43,40.7877194,-1.3553635),(513,'Becerrada en la plaza de toros','Becerrada en la plaza de toros, ¡ojo con las vacas que están más despejadicas que algunos! (entrada gratuita).',1,'2015-08-23','0800','',0,0,0,0,1,43,40.7877194,-1.3553635),(515,'Entierro de la sardina ','Estéis o no... haremos el entierro de la sardina y nos iremos a la última charanga de las fiestas ¡no os la perdáis!',7,'2015-08-27','0600','',8.5,2,0,0,1,43,40.7877194,-1.3553635),(516,'Charanga','Charanga lista y escobas preparadas, nos faltas tú.',2,'2015-08-26','0530','',0,0,0,0,1,43,40.7877194,-1.3553635),(644,'Feria de Caza y Pesca','La feria de caza, pesca y turismo rural, nos mostrará las últimas novedades e innovaciones relacionadas con el sector, siendo una feria de gran importancia tanto para profesionales como para los aficionados al mundo de la caza, pesca y turismo rura.\r\n\r\nCaptur 2015, contará con un gran programa de actividades paralelo, exhibiciones de caza con Liebres, campeonatos de recorridos de aza, Concurso de Pesca, Concentraciones de Rehalas, Concentración acional de 4x4, Camp',7,'2015-09-10','1515','',8,3,0,0,1,1,41.711934,-0.99766),(645,'Torneo  3x3 Baloncesto','Ha dado comienzo una nueva edición del Torneo.Las categorías de la competición serán Alevín: (nacidos en los años 2001-2002) y Benjamín: (nacidos en los años 2003-2004) y las modalidades de participación serán masculino, femenino y mixto. Los equipos estarán compuestos por cuatro componentes (3+1 suplente).',3,'2015-09-12','0215','',6,4,7,2,1,1,41.7114982,-0.9972644),(688,'Torneo 3x3 Baloncesto','Ha dado comienzo una nueva edición del Torneo.Las categorías de la competición serán Alevín: (nacidos en los años 2001-2002) y Benjamín: (nacidos en los años 2003-2004) y las modalidades de participación serán masculino, femenino y mixto. Los equipos estarán compuestos por cuatro componentes (3+1 suplente).',3,'2015-09-16','1000','',0,0,0,0,1,74,40.883186,-0.5768388),(723,'DSASC','CxzCXZC',3,'2015-09-26','0300','',0,0,0,0,1,77,40.883186,-0.5768388),(804,'Exposición: Espacio al cubo','Inauguración de la exposición Espacio al cubo, del pintor Luis Loras, ganador del Certamen Lapayese 2014',7,'2015-11-14','1830','',0,0,0,0,1,41,40.7903605,-1.3532413),(830,'Taller teórico-practico de \"mindfulness\".','Taller teórico-practico de \"mindfulness\" en el colegio Castillo Qadrit, dirigido a padres y madres, impartido por Sara Royo y Raquel deVilla-ceballos',3,'2015-12-11','1500','1700',10,3,0,0,1,96,41.5407007,-0.9941023),(831,'III Carrera solidaria  de Navidad organizada por el \"Club Running Cadrete\".','III Carrera solidaria  de Navidad organizada por el \"Club Running Cadrete\".\r \r 10:30: Carreras infantiles.\r \r 12:00: Carrera 6K (precio solidario inscripción 5 euros).\r Al finalizar la carrera haremos una chocolatada para comenzar de la mejor manera posible la navidad de este 2015.',3,'2015-12-19','1030','',10,3,0,0,1,96,41.5549572,-0.9598921),(832,'\"XXXIII Juegos escolares\".','\"XXXIII Juegos escolares\" en el pabellón polideportivo, campeonato de ajedrez. (Inscripciones hasta el día 15).',3,'2015-12-19','1800','',10,3,0,0,1,96,41.5574557,-0.9629263),(833,'X Encuentro de Corales y Coros en la Iglesia parroquial. ','X Encuentro de Corales y Coros en la Iglesia parroquial. \r\n\r\nContaremos con:\r\n\r\n-Coral de Longares\r\n-Coral \"San Antonio de Padua\" de Mezalocha\r\n-Asociación Cultural Coral \"Cantores de María de Huerva\"\r\n-Grupo Vocal \"Gaia Nueva\" (Zaragoza)\r\n-Coro \"Vexilla Regis\" (Zaragoza)\r\n-Coral de Cadrete',2,'2015-12-19','1800','',10,3,0,0,1,96,41.5552136,-0.9599324),(834,'Campeonato de Aragón de ajedrez relámpago por equipos.','Campeonato de Aragón de ajedrez relámpago por equipos  en el pabellón polideportivo.',3,'2015-12-20','1000','',10,3,0,0,1,96,41.5574557,-0.9629263),(835,'Exhibición  escuelas deportivas.','Exhibición  escuelas deportivas en el pabellón polideportivo.\r\n',3,'2015-12-20','1600','',10,3,0,0,1,96,41.5549572,-0.9598921),(836,'La Coral de Cadrete y la Escuela municipal de jotas visitarán la residencia de mayores Vitalia.','La Coral de Cadrete y la Escuela municipal de jotas visitarán la residencia de mayores Vitalia.',2,'2015-12-21','1700','',9.75,4,0,0,1,96,41.5553673,-0.9611608),(837,'Tronca de Nadal.','Tronca de Nadal en el pabellón polideportivo organizada por la Asociación Cultural \"A Gardincha\".',4,'2015-12-22','1800','',10,3,0,0,1,96,41.5574557,-0.9629263),(838,'Jornada deportiva multigeneracional.','Jornada deportiva multigeneracional con juegos de puntería, buble soccer, pistolas láser... y muchas sorpresas más, en el pabellón polideportivo.',3,'2015-12-23','1600','',10,3,0,0,1,96,41.5574557,-0.9629263),(839,'Este año Papa Noel visitara Cadrete.','Este año Papa Noel visitara Cadrete para que los más peques se hagan fotos con el y le pidan los regalos, estará en la entrada del Centro Socicultural.',4,'2015-12-24','1800','',10,3,0,0,1,96,41.55535,-0.96067),(840,'Macro-Discomovil \"Pegassus\".','Macro-Discomovil \"Pegassus\" en el pabellón polideportivo.',2,'2015-12-24','2355','',10,3,0,0,1,96,41.5574557,-0.9629263),(841,'Cine.','Cine, proyección de la película \"Los Minions\" en el Salón de Actos del Ayuntamiento.',7,'2015-12-26','1800','',10,3,0,0,1,96,41.5549325,-0.9602827),(842,'Recogida de cartas para Sus Majestades los Reyes Magos.','Un emisario de Sus Majestades los Reyes Magos estará en la entrada del Centro Socicultural para recoger las cartas.',4,'2015-12-27','1700','',10,3,0,0,1,96,41.55535,-0.96067),(843,'Campanadas infantiles.','Campanadas infantiles \"Menudo Año Nuevo\" en la Plaza de Aragón. Música, reparto de uvas (muy especiales), personajes infantiles y sobretodo mucha alegría.',4,'2015-12-31','1100','',10,3,0,0,1,96,41.5549572,-0.9598921),(844,'Noche Vieja.','Uvas, champan y sorteo de un jamón, en la plaza, a continuación baile con la orquesta \"Nueva Samurai\" en el pabellón, al finalizar discomovil.',7,'2015-12-31','2355','',10,3,0,0,1,96,41.5549572,-0.9598921),(845,'Vermú jotero.','Vermú jotero en la 1ª planta del Centro Socicultural, Castañuelas de Aragón nos amenizará con unas divertidas jotas, ven y disfruta de tu tapa o bebida pasando un buen rato.',2,'2016-01-03','1230','',10,2,0,0,1,96,41.55535,-0.96067),(846,'\"Superheroes y Muñecos\".','\"Superheroes y Muñecos\" espectáculo familiar en el pabellón, venta anticipada en el Ayuntamiento y el mismo día en el pabellón.',4,'2016-01-03','1900','',10,2,0,0,1,96,41.5574557,-0.9629263),(847,'Concierto del cantautor Karlos Zuazo.','Concierto del cantautor Karlos Zuazo, en el Salón de Actos del Ayuntamiento, con su repertorio \"Amar es el verbo más bello\".',2,'2016-01-03','2030','',10,1,0,0,1,96,41.5549325,-0.9602827),(848,'Cuentacuentos y taller.','Cuentacuentos y taller en la ludoteca municipal.',4,'2016-01-04','1800','',10,2,0,0,1,96,41.5546008,-0.9594596),(849,'Cabalgata de Reyes.','Cabalgata de Reyes, salida desde el pabellón, recorrido por las calles del municipio con parada en la Iglesia donde Sus Majestades adoraran al niño, para acabar de nuevo en el pabellón y que Sus Majestades de oriente repartan los regalos, este año vendrán en un transporte muy especial...   \r (El pabellón polideportivo permanecerá abierto durante toda la mañana).',4,'2016-01-05','1800','',9.5,4,0,0,1,96,41.5574557,-0.9629263),(850,'Macro-Discomovil \"Pegassus\".','Macro-Discomovil \"Pegassus\" en el pabellón.',2,'2016-01-05','2355','',10,3,0,0,1,96,41.5574557,-0.9629263),(851,'Cuentacuentos y taller.','Cuentacuentos y taller en la biblioteca municipal.',4,'2016-01-07','1800','',10,2,0,0,1,96,41.55535,-0.96067),(852,'Fiesta Invernal de las Culturas.','Fiesta Invernal de las Culturas en el pabellón de 11:00 a 14:00 y de 17:00 a 21:00\r (Música, gastronomía internacional, artesanía, pintacaras...) ENTRADA GRATUITA',5,'2016-01-09','1100','2100',10,2,0,0,1,96,41.5574557,-0.9629263),(853,'Cine.','Cine, proyección de la película \"Jurassic World\" en el Salón de Actos del Ayuntamiento.',7,'2016-01-10','1800','',10,2,0,0,1,96,41.5549325,-0.9602827),(854,'Concurso de balcones,  ventanas y fachadas.','Concurso de ventanas, fachadas y balcones. Apuntarse en el Ayuntamiento antes del día 22 de diciembre.',7,'2015-12-22','1430','',10,4,0,0,1,78,41.5549325,-0.9602827),(855,'Concurso de Belenes.','Concurso de Belenes. Apuntarse en el Ayuntamiento antes del día 22 de diciembre.',7,'2015-12-22','1430','',10,2,0,0,1,78,41.5549325,-0.9602827),(856,'Rastrillo solidario de libros','Rastrillo solidario de libros de segunda mano, del 12 al 19 de diciembre en la entrada del Centro Socicultural. Entre semana de 10:00 a 13:00 y de 17:00 a 20:00, sábados de 10:00 a 14:00.',7,'2015-12-12','1000','',10,4,0,0,1,78,41.55535,-0.96067),(857,'Torneo de Navidad Fútbol7','Estamos preparando el tradicional Torneo de Fútbol 7 de Navidad.\r Tendrá lugar en el campo de fútbol de nuestra localidad el sábado 26 de Diciembre, desde las 9h de la mañana hasta finalizar el torneo.\r Haz tu equipo y apúntate!!! Cuanto antes ya que las plazas son limitadas (Máximo 10 equipos).\r La inscripción serán 50euros por equipo...para más información llamar al tlfn. 646240050 o en el correo serviciodedeportes@monrealdelcampo.com',3,'2015-12-26','1000','2000',0,0,0,0,1,41,40.7899459,-1.3488493),(884,'Cabalgata de Reyes.','Cabalgata de Reyes, salida desde el pabellón, recorrido por las calles del municipio con parada en la Iglesia donde Sus Majestades adoraran al niño, para acabar de nuevo en el pabellón y que Sus Majestades de oriente repartan los regalos, este año vendrán en un transporte muy especial...   \r\n (El pabellón polideportivo permanecerá abierto durante toda la mañana).',2,'2015-12-25','1500','',7,2,0,0,1,1,41.7114982,-0.9972644),(888,'IV MERCADILLO ARTESANÍA.',' Pabellón de  11 a 14h y de16 a 20h.\r\n 12 h CUENTACUENTOS, TODO CAMBIA EN NAVIDAD. CHISPANDORA. (PROGRAMA DE INVIERNO DE BARRIOS RURALES)\r\n17 h CONCIERTO DE NAVIDAD ALUMNOS MÚSICA CASA DE JUVENTUD.\r\n19 H GRUPO DE BAILE MEDIEVAL OCTAVA MILLA DE UTEBO\r\nOrganiza: C. de Cultura\r\nColabora:  C. Cívico y Casa de Juventud\r\n',7,'2015-12-20','1100','',0,0,0,0,1,47,41.72191285708473,-1.0245201870504883),(889,'CONCIERTO NAVIDEÑO','CONCIERTO NAVIDEÑO  DE LA BANDA \" ARMONÍA ARTÍSTICA\" a las 12H en C.Cívico. \r\nPatrocina: A.VV. Casetas.\r\n',2,'2015-12-20','1200','',0,0,0,0,1,47,41.71914108768745,-1.0274230077568358),(890,'CINE DE BARRIO. \"LA CASA MAGICA\"','CINE DE BARRIO. \"LA CASA MAGICA  \". C. Cívico,18h. Con invitación\r\n',7,'2015-12-20','1800','',0,0,0,0,1,47,41.71914108768745,-1.0274230077568358),(891,'MUESTRA DE MÚSICA Y TEATRO  ','MUESTRA DE MÚSICA Y TEATRO  a las 19:30 h en C.Cívico.\r\n\r\nORGANIZA: C.T.L. ARIANTA\r\n',2,'2015-12-21','1930','',0,0,0,0,1,47,41.71914108768745,-1.0274230077568358),(892,'FESTIVAL DE NAVIDAD ESCUELA DE MÚSICA','FESTIVAL DE NAVIDAD ESCUELA DE MÚSICA a las 18h en C.Cívico.\r\nORGANIZA: ESCUELA DE MÚSICA DE CASETAS\r\n',2,'2015-12-22','1800','',0,0,0,0,1,47,41.71914108768745,-1.0274230077568358),(893,'TALLER DE ADORNOS NAVIDEÑOS PARA EL ÁRBOL','TALLER DE ADORNOS NAVIDEÑOS PARA EL ÁRBOL\r\ndirigida a niños de 4 a 8 años (menores de 4 años acompañados de un adulto)  de 17 a 19 h en C.Cívico. Plazas limitadas\r\nORGANIZA: ASOCIACIÓN CULTURAL COFISA.\r\n',7,'2015-12-23','1700','1900',0,0,0,0,1,47,41.71914108768745,-1.0274230077568358),(894,'DIEGO ESCUSOL. Cantautor','DIEGO ESCUSOL.Cantautor sin pelos en la lengua.20:30h.Entrada; Taquilla inversa.Taberna Vinos Chueca.',2,'2015-12-23','2030','',0,0,0,0,1,47,41.71890204323232,-1.027020676404602),(895,'CINEFORUM CASETAS','CINEFORUM CASETAS presenta: \"LO QUE HACEMOS EN LAS SOMBRAS\". C.Cívico a las 18 h.\r\nSinopsis: Estas Navidades, Cineforum Casetas quiere que pases un rato divertido conociendo a fondo la vida de los vampiros en Nueva Zelanda. Todas las preguntas que te has estado haciendo sobre esta especie quedan respondidas en \"Lo que hacemos en las sombras\", película-documental imprescindible para admiradores, amigos, enemigos y esclavos de los vampiros.\r\n',7,'2015-12-26','1800','',0,0,0,0,1,47,41.71914108768745,-1.0274230077568358),(896,'GRAN CONCIERTO DE NAVIDAD','GRAN CONCIERTO DE NAVIDAD DE LA CAPILLA DE MÚSICA Nª SRA DEL PILAR bajo la dirección del maestro JOSÉ MARIA BERMEJO. Parroquia de San Miguel a las 19:30',2,'2015-12-26','1930','',0,0,0,0,1,47,41.7181368,-1.0260382),(897,'CINE DE BARRIO: EL APRENDIZ DE PAPA NOEL','SESIÓN MATINAL DE CINE DE BARRIO: \" EL APRENDIZ DE PAPA NOEL\".C. Cívico 11:30h. Con invitación.\r\n',7,'2015-12-27','1130','',0,0,0,0,1,47,41.71914108768745,-1.0274230077568358),(898,'CONCIERTO DE LA ESCUELA DE MÚSICA MODERNA','CONCIERTO DE LA ESCUELA DE MÚSICA MODERNA. C.Cívico a las 17 h.\r\nTu invitación por un euro solidario desde una hora antes del comienzo.\r\n',2,'2015-12-27','1700','',0,0,0,0,1,47,41.71914108768745,-1.0274230077568358),(899,'GRAN MERCADILLO DEL HUERVA','Inauguración del GRAN MERCADILLO DEL HUERVA, más de 60 puestos, ropa, bisutería, menaje, comida... un tren turístico gratuito, hinchables, música y muchas sorpresas más. Se realizará en el aparcamiento de la calle Miguel Servet.',5,'2016-01-09','0900','1400',10,4,0,0,1,78,41.5407007,-0.9941023),(900,'Prueba #1','',1,'2015-08-27','1320','',0,0,0,0,1,25,41.7148536,-0.9946819),(901,'Feria Infantil de Navidad','Hinchables, manualidades, gymkanas, escalada y mucha diversión en el pabellón',4,'2015-12-28','1700','2000',0,0,0,0,1,41,40.7899459,-1.3488493),(902,'Feria Infantil de Navidad','Hinchables, manualidades, gymkanas, escalada y mucha diversión en el pabellón',4,'2015-12-29','1700','2000',0,0,0,0,1,41,40.7899459,-1.3488493),(903,'Cabalgata de Reyes','17:30h Chocolate\r\n17:30h Cabalgata de Reyes\r\n',4,'2016-01-05','1700','',0,0,0,0,1,41,40.7903605,-1.3532413),(904,'Carrera de San Silvestre','SALIDA Y LLEGADA EN LA PLAZA DE ESPAÑA\r\n(Vuelta popular, no competitiva con regalos para todos)\r\n',3,'2015-12-31','1700','',0,0,0,0,1,41,40.7867214,-1.3540693),(905,'evento 1','',3,'2015-12-29','0320','',0,0,0,0,1,98,41.7172407,-0.9950091),(906,'evento 2','',2,'2015-12-30','0105','',0,0,0,0,1,98,41.7172407,-0.9950091),(907,'evento 13','',3,'2015-12-29','0320','',0,0,0,0,1,98,41.7172407,-0.9950091);
/*!40000 ALTER TABLE `Evento` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER `Evento_AFTER_INSERT` AFTER INSERT ON `Evento` FOR EACH ROW
BEGIN 
     DECLARE entero INT;
     SELECT idPueblo into entero from Fiesta where idFiesta = NEW.idFiesta;
	 UPDATE Pueblo set Version = Version + 1 where idPueblo = entero;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER `Evento_AFTER_UPDATE` AFTER UPDATE ON `Evento` FOR EACH ROW
BEGIN
     DECLARE entero INT;
     SELECT idPueblo into entero from Fiesta where idFiesta = NEW.idFiesta;
	 UPDATE Pueblo set Version = Version + 1 where idPueblo = entero;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER `Evento_AFTER_DELETE` AFTER DELETE ON `Evento` FOR EACH ROW
BEGIN
     DECLARE entero INT;
     SELECT idPueblo into entero from Fiesta where idFiesta = OLD.idFiesta;
	 UPDATE Pueblo set Version = Version + 1 where idPueblo = entero;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Fiesta`
--

DROP TABLE IF EXISTS `Fiesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Fiesta` (
  `idFiesta` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(40) NOT NULL,
  `Inicio` date DEFAULT NULL,
  `Fin` date DEFAULT NULL,
  `idPueblo` int(11) NOT NULL,
  PRIMARY KEY (`idFiesta`),
  KEY `fk_Fiesta_Pueblo_idx` (`idPueblo`),
  CONSTRAINT `fk_Fiesta_Pueblo` FOREIGN KEY (`idPueblo`) REFERENCES `Pueblo` (`idPueblo`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Fiesta`
--

LOCK TABLES `Fiesta` WRITE;
/*!40000 ALTER TABLE `Fiesta` DISABLE KEYS */;
INSERT INTO `Fiesta` VALUES (1,'NULA','2015-01-01','2050-01-01',1),(25,'Santa Ana','2015-08-24','2015-08-27',1),(41,'NULA','2015-01-01','2050-01-01',2),(43,'Programa De Fiestas','2015-08-20','2015-08-27',2),(44,'NULA','2015-01-01','2050-01-01',3),(47,'NULA','2015-01-01','2050-01-01',4),(74,'NULA','2015-01-01','2050-01-01',6),(77,'Asd','2015-09-26','2015-09-27',6),(78,'NULA','2015-01-01','2050-01-01',7),(92,'NULA','2015-01-01','2050-01-01',8),(96,'Navidades 2015','2015-12-11','2016-01-10',7),(97,'NULA','2015-01-01','2050-01-01',9),(98,'Programa','2015-12-29','2015-12-31',1);
/*!40000 ALTER TABLE `Fiesta` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER `dfef`.`Fiesta_AFTER_UPDATE` AFTER UPDATE ON `Fiesta` FOR EACH ROW
BEGIN
	UPDATE Pueblo set Version = Version + 1 where idPueblo = NEW.idPueblo;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER `dfef`.`Fiesta_AFTER_DELETE` AFTER DELETE ON `Fiesta` FOR EACH ROW
BEGIN
	UPDATE Pueblo set Version = Version + 1 where idPueblo = OLD.idPueblo;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Lugar`
--

DROP TABLE IF EXISTS `Lugar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Lugar` (
  `Nombre` varchar(50) NOT NULL,
  `Latitud` double NOT NULL,
  `Longitud` double NOT NULL,
  `Pueblo_idPueblo` int(11) NOT NULL,
  PRIMARY KEY (`Latitud`,`Longitud`,`Pueblo_idPueblo`),
  KEY `fk_Lugar_Pueblo1_idx` (`Pueblo_idPueblo`),
  CONSTRAINT `fk_Lugar_Pueblo1` FOREIGN KEY (`Pueblo_idPueblo`) REFERENCES `Pueblo` (`idPueblo`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lugar`
--

LOCK TABLES `Lugar` WRITE;
/*!40000 ALTER TABLE `Lugar` DISABLE KEYS */;
INSERT INTO `Lugar` VALUES ('Sin Lugar',0,0,2),('Sin Lugar',0.000006666,0.000006666,1),('Sin Lugar',0.00002,0.00002,3),('No Lugar',0.00003333,0.00003333,6),('No Lugar',0.000039996,0.000039996,7),('No Lugar',0.000046662,0.000046662,8),('No Lugar',0.000053328,0.000053328,9),('Sin Lugar',1,1,4),('Avenida madrid',40.7863371,-1.3538412,2),('Plaza España',40.7867214,-1.3540693,2),('AVENIDA',40.7868292,-1.356047,2),('A',40.7877194,-1.3553635,2),('Avenida zaragoza',40.7880207,-1.3534633,2),('Virgen del Carmen ',40.7899459,-1.3488493,2),('Plaza Mayor',40.7903605,-1.3532413,2),('Casa de Cultura, Plaza Mayor 10',40.7903683,-1.3525465,2),('Mayor',40.7905362,-1.3529612,2),('Calle Mayor Alta',40.883186,-0.5768388,6),('F',40.8833765,-0.5760288,6),('3432',40.9774572,-0.4532204,6),('123',40.9782922,-0.4550992,6),('Miguel Servet, 3',41.5407007,-0.9941023,7),('Calle Joaquin Costa, 4',41.5546008,-0.9594596,7),('Plaza de Aragón, 5',41.5549325,-0.9602827,7),('Plaza de Aragón',41.5549572,-0.9598921,7),('Plaza de Aragón, 1',41.5552136,-0.9599324,7),('Tenor FLeta, 7',41.55535,-0.96067,7),('Calle Osera, 6',41.5553673,-0.9611608,7),('Avenida Zaragoza, 2',41.5568476,-0.9618349,7),('Avenida Zaragoza, 4',41.5574557,-0.9629263,7),('Santo Cristo, 2',41.5579967,-0.9580277,7),('Nula',41.5976275,-0.9056623,1),('Avenida zaragoza 28',41.5976275,-0.9056623,3),('D',41.6389425,-0.9825329,1),('Avenida zARAGOZA 60',41.7047822,-0.9992055,1),('calle goya',41.70509221444853,-0.9981939702931908,1),('Plaza Zaragoza',41.7052505,-0.9986491,1),('Calle Teruel 2',41.7058719,-0.9980192,1),('Calle huesca',41.7060976,-0.9965078,1),('Calle Teruel 3',41.7062336,-0.9982014,1),('Plaza Europa',41.7063095,-0.9984257,1),('Avenida Zaragoza',41.7066422,-0.9986026,1),('Avenida zaragoza',41.7067257,-0.998605,1),('Paseo de los prados',41.7070751,-0.9946534,1),('Avenida zaragoza 34',41.7071117,-0.9985673,1),('Avenida zaragoza 29',41.7073171,-0.9984356,1),('Avenida zaragoza 30',41.70742,-0.99874,1),('Avenida zaragoza 28',41.7077348,-0.9986387,1),('Avenida zaragoza 24',41.7078932,-0.9986508,1),('Avenida Zaragoza 21',41.707928,-0.9985216,1),('avenida Zaragoza 28',41.70795970339997,-0.9985842317048577,1),('Avenida Navarra',41.7082398,-1.000588,1),('Sfsdfser',41.7087277,-0.9986738,1),('Plaza Aragon',41.7087557,-0.9986465,1),('Avenida Zaragoza 3',41.7092931,-0.9982107,1),('ayuntamiento',41.70975932781371,-0.9980927169028786,1),('Avenida Zaragoza 1',41.709883,-0.9978591,1),('Aranjuez',41.7114982,-0.9972644,1),('Madrid',41.711934,-0.99766,1),('Plaza Santa Ana',41.7130425,-0.9967132,1),('Calle Soria 2',41.7137866,-0.9972361,1),('13',41.7148536,-0.9946819,1),('plaza España',41.7149231442037,-0.9948459028903511,1),('Vi',41.7150009,-0.994956,1),('Campo de Futbol',41.71715102566825,-1.0344262554947203,4),('123',41.7172407,-0.9950091,1),('Iglesia',41.7181368,-1.0260382,4),('Plaza Jose Las Heras',41.718278201163784,-1.0294128715697792,4),('Bar Pozal',41.71847740689422,-1.0260604455772704,4),('Nula',41.7186518,-1.0370704,4),('Plaza Dolotea Arnal',41.718812091033314,-1.0286219551864928,4),('Alcaldía vieja',41.71881675582671,-1.0258834197822875,4),('Taberna Vinos Chueca',41.71890204323232,-1.027020676404602,4),('Calle Palacio',41.7190874,-1.0262894,4),('Centro Cívico',41.71914108768745,-1.0274230077568358,4),('Plaza Santiago Castillo',41.71920014793726,-1.027374057442334,4),('Calle San Miguel',41.71934879935873,-1.0254609718624619,4),('Parque Clemente Mateo',41.71950946262704,-1.0319049790207213,4),('Plaza España',41.719601055524215,-1.026972061366227,4),('Calle Galicia',41.71980175863074,-1.0277046397034195,4),(' Calle Alcubierre, 32, 50620 Zaragoza',41.7204055,-1.0282549,4),('Plaza Constitucion',41.7204981,-1.0253394,1),('Bar Los Arcos',41.72143338657359,-1.0290051757995156,4),('Avenida Constitución 19',41.7218997,-1.0245542,4),('Pabellón',41.72191285708473,-1.0245201870504883,4),('Avenida constitución',41.7227526,-1.0236009,1),('Plaza De Toros',41.722828745936724,-1.026470823556549,4);
/*!40000 ALTER TABLE `Lugar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Pueblo`
--

DROP TABLE IF EXISTS `Pueblo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pueblo` (
  `idPueblo` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(44) NOT NULL,
  `Latitud` double NOT NULL,
  `Longitud` double NOT NULL,
  `Version` int(11) DEFAULT NULL,
  `Usuario` varchar(10) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL,
  `Comunidad_Autonoma` varchar(24) NOT NULL,
  `Habilitado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idPueblo`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Pueblo`
--

LOCK TABLES `Pueblo` WRITE;
/*!40000 ALTER TABLE `Pueblo` DISABLE KEYS */;
INSERT INTO `Pueblo` VALUES (1,'Muestra',41.71011924664712,-0.9982700779739684,693,'qwe','qwe','ARAGON',1),(2,'Monreal del Campo',40.79056320653722,-1.3529610395137137,147,'kiz5n','dskc3','ARAGON',1),(3,'xñprueba',39.94535,0.354343,188,'asd','asd','ARAGON',1),(4,'Casetas',41.71915009687781,-1.0274156316820648,212,'fhw42','bnsj3','ARAGON',1),(6,'Crivillen',40.88317595241375,-0.5768253042999572,79,'sdf','sdf','ARAGON',1),(7,'Cadrete',41.55494452475492,-0.959726734429982,313,'8o7ds','axow3','ARAGON',1),(8,'Alfajarin',41.61379967058007,-0.7032324507538146,1,'asdkh','sgwde','ARAGON',1),(9,'Madrid',41.71011924664712,41.71011924664712,6,'aas','dfe','MADRID',1);
/*!40000 ALTER TABLE `Pueblo` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER `dfef`.`Pueblo_AFTER_INSERT` AFTER INSERT ON `Pueblo` FOR EACH ROW
BEGIN
DECLARE num int;
	Insert Into Fiesta values (null, 'NULA','2015-01-01','2050-01-01', NEW.idPueblo);
    set num = (select count(*) from Pueblo);
    Insert into Lugar values ('No Lugar',num * (1/150000),num * (1/150000),New.idPueblo);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Tablon`
--

DROP TABLE IF EXISTS `Tablon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tablon` (
  `idTablon` int(11) NOT NULL AUTO_INCREMENT,
  `texto` varchar(300) DEFAULT NULL,
  `Tipo` tinyint(1) DEFAULT NULL,
  `Pueblo_idPueblo` int(11) NOT NULL,
  `Evento_idEvento` int(11) DEFAULT NULL,
  `extra` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idTablon`),
  KEY `fk_Tablon_Pueblo1_idx` (`Pueblo_idPueblo`),
  KEY `fk_Tablon_Evento1_idx` (`Evento_idEvento`),
  CONSTRAINT `fk_Tablon_Evento1` FOREIGN KEY (`Evento_idEvento`) REFERENCES `Evento` (`idEvento`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Tablon_Pueblo1` FOREIGN KEY (`Pueblo_idPueblo`) REFERENCES `Pueblo` (`idPueblo`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=580 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tablon`
--

LOCK TABLES `Tablon` WRITE;
/*!40000 ALTER TABLE `Tablon` DISABLE KEYS */;
INSERT INTO `Tablon` VALUES (434,'se cancela el toro por causas meteorológicas. Lo pasamos al miércoles 26 a las 11 antes de los fuegos',5,2,445,''),(435,'metereológico ',3,2,452,'pabellón'),(490,'Debido al accidente producido en las inmediaciones.',3,1,644,'zona de pesca muy alta'),(491,'Esperamos a que llegue la ambulancia para dar comienzo al evento',4,1,161,''),(492,'Se cierran las calles del evento debido a la riada',5,1,165,''),(493,'porque nosmos quedado',5,1,162,'asd'),(495,'Porque hace demasiado frio',5,1,179,''),(497,'Porque llovera',5,1,645,''),(551,'',3,2,804,'plaza Mayor '),(553,'hola',5,1,645,''),(554,'oooou',5,1,177,''),(555,'aamemeodbsufbskdjf fkfifnrnr f r t fifkmskdmfmkrnsk fykdowkekgogme fkgkfkfkfkrnsnd fkrirnfnjf fjfkdlwment fkeiengngnissodnrby rkfirntnkhosmsnf gjfkdnr g g g y tkdkkd g f',4,1,156,''),(556,'j',5,1,157,''),(557,'ui',5,1,153,''),(558,'p',5,1,158,''),(559,'iii',5,1,159,''),(560,'a',5,1,161,''),(561,'b',5,1,160,''),(562,'h',5,1,162,''),(563,'o',5,1,168,''),(564,'uu',5,1,169,''),(565,'u',5,1,163,''),(566,'ppp',5,1,164,''),(567,'d',5,1,165,''),(568,'ooohh',5,1,167,''),(579,'P',6,1,NULL,'');
/*!40000 ALTER TABLE `Tablon` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER actualizarVersion AFTER INSERT ON `Tablon` FOR EACH ROW
BEGIN
	UPDATE Pueblo set Version = Version + 1 where idPueblo = NEW.Pueblo_idPueblo;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER `dfef`.`Tablon_AFTER_UPDATE` AFTER UPDATE ON `Tablon` FOR EACH ROW
BEGIN
	UPDATE Pueblo set Version = Version + 1 where idPueblo = NEW.Pueblo_idPueblo;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`cliente`@`%`*/ /*!50003 TRIGGER `dfef`.`Tablon_AFTER_DELETE` AFTER DELETE ON `Tablon` FOR EACH ROW
BEGIN
	UPDATE Pueblo set Version = Version + 1 where idPueblo = OLD.Pueblo_idPueblo;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-09 14:03:18
