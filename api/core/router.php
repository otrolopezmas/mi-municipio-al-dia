<?php

/**
 * Mi Municipio al Día
 * 
 * @author Marcos Lopez
 * 
 * Router class
 * 
 * Keeps different routes for each HTTP method with information about what 
 * controller and action must be called
 */
class Router {

        public $routes = array(
                'get' => array(),
                'post' => array(),
                'put' => array(),
                'delete' => array()
        );

        /**
         * Check controller param is set
         * 
         * @param array $params
         */
        private function check_params(array $params) {

                if (!isset($params['controller'])) {
                        return false;
                }

                return $params;
        }

        /**
         * Replace normal characters into regular expression to posterior comparation
         * 
         * @param String $url original route
         * @param String $regex regex to replace
         * 
         * @return String with regex sintax
         */
        private function url_to_regex($url, $regex = null) {
                $url = str_replace("/", "\/", $url);
                if (!is_null($regex)) {
                        foreach ($regex as $key => $value) {
                                $url = str_replace($key, $value, $url);
                        }
                }

                return "(^$url$)";
        }

        /**
         * Save a route with method GET
         * 
         * @param type $pattern
         * @param callable $params
         * 
         * @return \Router
         */
        function get($pattern, array $params, array $regex = null) {

                if ($this->check_params($params) !== false) {
                        $this->routes['get'][$this->url_to_regex($pattern, $regex)] = $params;
                        return $this;
                } else {
                        return false;
                }
        }

        /**
         * Save a POST method route
         * 
         * @param type $pattern
         * @param array $params
         * @param array $regex
         * 
         * @return boolean|\Router
         */
        function post($pattern, array $params, array $regex = null) {
                if ($this->check_params($params) !== false) {
                        $this->routes['post'][$this->url_to_regex($pattern, $regex)] = $params;
                        return $this;
                } else {
                        return false;
                }
        }

        /**
         * Save a PUT method route
         * 
         * @param type $pattern
         * @param array $params
         * @param array $regex
         * 
         * @return boolean|\Router
         */
        function put($pattern, array $params, array $regex = null) {
                if ($this->check_params($params) !== false) {
                        $this->routes['put'][$this->url_to_regex($pattern, $regex)] = $params;
                        return $this;
                } else {
                        return false;
                }
        }

        /**
         * Save a DELETE method route
         * 
         * @param type $pattern
         * @param array $params
         * @param array $regex
         * 
         * @return boolean|\Router
         */
        function delete($pattern, array $params, array $regex = null) {
                if ($this->check_params($params) !== false) {
                        $this->routes['delete'][$this->url_to_regex($pattern, $regex)] = $params;
                        return $this;
                } else {
                        return false;
                }
        }

        /**
         * Check if the specified path exists in the object
         * 
         * @param Request $request contain petition's method and path
         * 
         * @return boolean | array on success
         */
        function match(Request $request) {
                $method = strtolower($request->get_method());

                if (!isset($this->routes[$method])) {
                        return false;
                }

                $path = $request->get_path();
                foreach ($this->routes[$method] as $pattern => $params) {
                        
                        //var_dump($pattern);
                        //var_dump($path);
                        

                        if (preg_match($pattern, $path)) {

                                $split = explode("/", $path);
                                $split = array_filter($split);
                                
                                foreach ($split as $key => $value) {
                                        if ($key % 2 != 0)
                                                continue;
                                        else
                                                $params['values'][] = $value;
                                }

                                return $params;
                        }
                }

                return false;
        }

}
