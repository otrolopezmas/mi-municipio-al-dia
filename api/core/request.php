<?php

/**
 * Mi Municipio al Día
 * 
 * @author Marcos Lopez
 * 
 * Request class
 * 
 * Keep information about the REST petition
 */
class Request {

        private $method;
        private $path;
        private $params;

        /**
         * 
         * @param type $method
         * @param type $path
         */
        function __construct($method, $path) {
                $this->method = $method;
                $this->path = $path;
        }

        public function get_method() {
                return $this->method;
        }

        function get_path() {
                return $this->path;
        }

        function get_params() {
                return $this->params;
        }

}
