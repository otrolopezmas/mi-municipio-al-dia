<?php

/**
 * Mi Municipio al Dia
 * 
 * @author Marcos Lopez
 * 
 * Base model class
 * 
 * Create here functions you want all your models to share
 */

class Model {

        protected $database;
        protected $table;

        public function __construct() {
                $this->database = new DB();
        }

        /**
         * Transform a string/date in format yyyy/mm/dd into dd-mm-yyyy
         * 
         * @param String $date
         * 
         * @return String formated date
         */
        public function format_out_date($date) {
                $date = str_replace("/", "-", $date);
                $datetime = new DateTime($date);
                return $datetime->format('d-m-Y');
        }

        /**
         * Transform a string/date with dd/mm/yyyy format into yyyy-mm-dd
         * 
         * @param String $date
         * 
         * @return String formated date
         */
        public function format_in_date($date) {
                $date = str_replace("/", "-", $date);
                $datetime = new DateTime($date);
                return $datetime->format('Y-m-d');
        }

        /**
         * Do a serie of changes in a given string
         * 
         * @param String $string
         * 
         * @return String formated string
         */
        public function clear_string($string) {
                $string = strtolower($string);
                $string = ucwords($string);
                $string = str_replace("€", "euro", $string);
                return $string;
        }

        public function __get($name) {
                return $name;
        }

}
