<?php

/**
 * Mi Municipio al Día
 * 
 * @author Marcos Lopez
 * 
 * Response Class
 * 
 * Generate personalized response (JSON FORMAT)
 * 
 * A reponse includes:
 * 1. HTTP status code
 * 2. JSON body that can be:
 *      - retrieved content
 *      - retrieved error
 * 
 */
class Response {

        private $body;
        private $status_code;
        private $user_message;
        private $dev_message;

        /**
         * Construct a personalized response
         * 
         * @param String | integer $status Status code
         * @param String $user_message Message for user
         * @param String $dev_message Message for techinal staff
         * @param boolean $auto_render on true print response automatically
         *      Set false if planning to add future messages
         */
        public function __construct($status = null, $content = null, $user_message = null, $dev_message = null, $auto_render = true) {

                $this->status_code = $status;

                if (!is_null($content)) {

                        $this->body = array(
                                'data' => array()
                        );

                        if (is_array($content)) {
                                foreach ($content as $key => $value) {
                                        $this->body['data'][$key] = $value;
                                }
                        } else {
                                $this->body ['data'] = $content;
                        }
                } else {
                        $this->message = $user_message;
                        $this->dev_message = $dev_message;

                        $this->body = array(
                                'error' => array(
                                        'message' => $user_message,
                                        'dev_message' => $dev_message
                                )
                        );
                }
                if ($auto_render)
                        $this->print_response();
        }

        /**
         * Add 
         * 
         * @param string $key
         * @param type $message
         */
        public function add_message($key, $message) {
                if (!isset($message)) {
                        $message = $key;
                        $key = 'general';
                }

                if (!isset($this->messages[$key])) {
                        $this->messages[$key] = array();
                }

                $this->messages[$key][] = $message;
        }

        /**
         * Retrieves a HTTP response with JSON content included
         */
        public function print_response() {

                http_response_code($this->status_code);

                echo json_encode($this->body, true);

                die();
        }

}
