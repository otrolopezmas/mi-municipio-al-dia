<?php

/**
 * Mi Municipio al Dia
 * 
 * @author Marcos Lopez
 * 
 * Front Controller class
 * 
 */
class front_controller {

        private $request;
        private $response;
        private $dispatcher;
        private $router;

        public function __construct() {
                ;
        }

        public function load_clases() {
                require_once 'db/db.php';
                require_once 'config/routes.php';
                require_once 'config/database.php';
                require_once 'core/dispatcher.php';
                require_once 'core/request.php';
                require_once 'controllers/login_controller.php';
        }

        public function check_login() {
                
                $login_controller = new login_controller();
                $pueblo = $login_controller->auth();

                if (!$pueblo) {
                        $response = new Response(401, null, 'Credenciales de autenticación erróneos', 'Unauthorized');
                }

                define('ID_PUEBLO', $pueblo['idPueblo']);
                define('NOMBRE_PUEBLO', $pueblo['Nombre']);
        }

}
