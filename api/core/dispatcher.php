<?php

/**
 * Mi Municipio al Dia
 * 
 * @author Marcos Lopez
 * 
 * Dispatcher class
 * 
 * Middleman between Router and Request
 */

class Dispatcher {

        private $router;

        /**
         * Initializes the object with a router object
         * 
         * @param Router $router
         */
        function __construct(Router $router) {
                $this->router = $router;
        }

        /**
         * Call route to verify path exists in our routes file
         * 
         * @param Request $request
         * 
         * @return array | boolean on success
         */
        function handle(Request $request) {
                $handler = $this->router->match($request);
                
                return (!$handler)? false: $handler;
        }

}
