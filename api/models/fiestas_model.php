<?php

/**
 * Mi Municipio al Dia
 * 
 * @author Marcos Lopez
 * 
 * Fiestas model
 * 
 * Data operations with table Fiesta
 */
require_once 'core/model.php';

class fiestas_model extends Model {

        public function __construct() {
                parent::__construct();
                $this->table = "Fiesta";
        }

        /**
         * Validate form
         * 
         * @param type $datos
         * 
         * @return boolean
         */
        private function validate_form() {

                //check we have the needed data

                $needed = array(
                        "nombre_fiesta", "inicio_fiesta", "fin_fiesta"
                );

                foreach ($needed as $key => $value) {
                        if (!isset($_POST[$value]))
                                $r = new Response(400, null, 'Hay errores en los datos', 'Validation error. Lacking of neccesary fields');
                }

                if (empty($_POST["nombre_fiesta"]))
                        $r = new Response(400, null, 'El nombre no puede estar vacío', 'Validation error. nombre_fiesta cannot be empty');

                $inicio = new DateTime($this->format_in_date($_POST['inicio_fiesta']));
                $fin = new DateTime($this->format_in_date($_POST['fin_fiesta']));

                $fin_bigger_than_inicio = ($inicio < $fin);

                if (!$fin_bigger_than_inicio)
                        $r = new Response(400, null, 'La fecha de finalización no puede ocurrir antes que la fecha de inicio', 'Validation error: fecha_fin cannot be sooner than fecha_inicio');

                /* if ($inicio < $fin)
                  return false; */

                return true;
        }

        /**
         * @Funcionalidad: transforma los datos del formulario en un input valido y
         * adecuado para la base de datos
         * @return type
         */
        private function adapt_form($id_fiesta = null) {

                //$nombre = $this->transform_string(filter_input(INPUT_POST, "nombre_fiesta",FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));
                //$fecha_inicio = $this->format_in_date(filter_input(INPUT_POST, "inicio_fiesta",FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));
                //$fecha_fin = $this->format_in_date(filter_input(INPUT_POST, "fin_fiesta",FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));

                /* $datos = [
                  ":nombre" => $nombre,
                  ":inicio" => $fecha_inicio,
                  ":fin" => $fecha_fin,
                  ":id_pueblo" => ID_PUEBLO
                  ]; */

                $datos = array(
                        ":nombre" => $this->clear_string($_POST['nombre_fiesta']),
                        ":inicio" => $this->format_in_date($_POST['inicio_fiesta']),
                        ":fin" => $this->format_in_date($_POST['fin_fiesta']),
                        ":id_pueblo" => ID_PUEBLO
                );

                if (!is_null($id_fiesta)) {
                        $datos["id_fiesta"] = $id_fiesta;
                }

                return $datos;
        }

        /**
         * Call database to obtain all fiestas from a specific pueblo
         * 
         * @return $result
         */
        public function get_fiestas() {

                $this->database->connect();

                $sql = "SELECT idFiesta, Nombre, Inicio, Fin FROM Fiesta WHERE idPueblo = :id_pueblo AND Nombre <> 'NULA' ORDER BY Inicio;";
                $data = [
                        ":id_pueblo" => ID_PUEBLO,
                ];
                $result = $this->database->select($sql, $data);

                if (!empty($result)) {

                        if (!isset($result[0])) {
                                $result = array(
                                        $result
                                );
                        }

                        for ($i = 0, $long = count($result); $i < $long; $i++) {
                                
                                $result[$i]["Inicio"] = $this->format_out_date($result[$i]["Inicio"]);
                                $result[$i]["Fin"] = $this->format_out_date($result[$i]["Fin"]);
                        }
                }

                $this->database->close();

                return $result;
        }

        public function get_fiesta($id_fiesta) {
                $this->database->connect();

                $sql = "SELECT idFiesta, Nombre, DATE_FORMAT(Inicio,'%d/%m/%Y') AS Inicio, DATE_FORMAT(Fin,'%d/%m/%Y') AS Fin, DATEDIFF(Fin, Inicio) AS Diff FROM Fiesta WHERE idPueblo = :id_pueblo AND idFiesta = :id_fiesta;";
                $data = [
                        ":id_pueblo" => ID_PUEBLO,
                        ":id_fiesta" => $id_fiesta
                ];
                $result = $this->database->select($sql, $data);

                if (!empty($result)) {

                        if (!isset($result[0])) {
                                $result = array(
                                        $result
                                );
                        }

                        for ($i = 0, $long = count($result); $i < $long; $i++) {
                                $result[$i]["Inicio"] = $this->format_out_date($result[$i]["Inicio"]);
                                $result[$i]["Fin"] = $this->format_out_date($result[$i]["Fin"]);
                        }
                }

                $this->database->close();

                return $result;
        }
        
        /**
         * 
         * @return type
         */
        public function get_fiesta_nula() {
                $this->database->connect();

                $sql = "SELECT idFiesta FROM Fiesta WHERE idPueblo = :id_pueblo AND Nombre = 'NULA'";
                $data = [
                        ":id_pueblo" => ID_PUEBLO,
                ];
                $result = $this->database->select($sql, $data);

                if (!empty($result)) {

                        if (!isset($result[0])) {
                                $result = array(
                                        $result
                                );
                        }
                }

                $this->database->close();

                return $result;
        }

        /**
         * Insert a fiesta in the database
         */
        public function insert_fiesta() {

                $sql = "INSERT INTO Fiesta VALUES(NULL, :nombre, :inicio, :fin, :id_pueblo);";
                if ($this->validate_form()) {


                        $datos = $this->adapt_form();
                        $this->database->connect();
                        $this->database->no_select($sql, $datos);
                        $fiestas = $this->get_fiestas();
                        $this->database->close();

                        return $fiestas;
                }
        }

        /**
         * Update a fiesta in the database
         */
        public function edit_fiesta($id_fiesta) {
                $sql = "UPDATE Fiesta SET Nombre=:nombre, Inicio=:inicio, Fin=:fin "
                        . "WHERE idFiesta = :id_fiesta AND idPueblo = :id_pueblo;";

                $datos = $this->adapt_form($id_fiesta);

                $this->database->connect();
                $result = $this->database->no_select($sql, $datos);
                //$fiestas = $this->get_fiestas();
                $this->database->close();

                return $result;
        }

        /**
         * Delete a fiesta in the database
         * 
         * @param String $id_fiesta
         */
        public function delete_fiesta($id_fiesta) {
                $sql = "DELETE FROM Fiesta WHERE idFiesta = :id_fiesta AND idPueblo = :id_pueblo";
                $datos = [
                        ":id_fiesta" => $id_fiesta,
                        "id_pueblo" => ID_PUEBLO
                ];

                $this->database->connect();
                $this->database->no_select($sql, $datos);
                $fiestas = $this->get_fiestas();
                $this->database->close();

                return $fiestas;
        }

}
