<?php

/**
 * Mi Municipio al Día
 * 
 * @author Marcos Lopez
 * 
 * Lugar model
 */

require_once 'core/model.php';
require_once 'inc/lugar.php';

class lugares_model extends Model {

        public function __construct() {
                parent::__construct();
                $this->table = "Fiesta";
        }

        public function get_lugares() {
                $this->database->connect();

                $sql = "SELECT Nombre, concat_ws('x',Latitud, Longitud)AS Coordenadas FROM Lugar WHERE Pueblo_idPueblo = :id_pueblo AND Nombre <> :nombre";
                $data = array(
                        ":id_pueblo" => ID_PUEBLO,
                        ':nombre' => 'Sin Lugar'
                );

                $result = $this->database->select($sql, $data);

                $this->database->close();
                
                return $result;
        }

        /**
         * Insert a new row for table Lugar
         * 
         * @param String $lugar
         * 
         * @return array
         */
        public function add_lugar($lugar) {
                $sql = "INSERT INTO Lugar VALUES(:nombre, :lat, :long, :id_pueblo);";
                
                $coords = Lugar::get_coords($lugar, NOMBRE_PUEBLO, COMUNIDAD_AUTONOMA);
                
                $this->database->connect();
                $this->database->no_select($sql, array(
                        ':nombre' => $lugar,
                        ':lat' => $coords["lat"],
                        ':long' => $coords["long"],
                        ':id_pueblo' => ID_PUEBLO
                ));
                $fiestas = $this->get_lugares();
                $this->database->close();

                return $fiestas;
        }

        public function delete_lugar($lat, $long) {
                $sql = "DELETE FROM Lugar WHERE Latitud = :lat AND Longitud = :long AND Pueblo_idPueblo = :id_pueblo";
                
                $this->database->connect();
                $this->database->no_select($sql, array(
                        ':lat' => $lat,
                        ':long' => $long,
                        ':id_pueblo' => ID_PUEBLO
                ));
                $fiestas = $this->get_lugares();
                $this->database->close();

                return $fiestas;
        }

}
