<?php

/**
 * Mi Municipio al Dia
 * 
 * @author Marcos Lopez
 * 
 * Login model
 */
require_once 'core/model.php';

class login_model extends Model {

        public function __construct() {
                parent::__construct();
                $this->table = "Pueblo";
        }

        /**
         * Check the existence of a user / Pueblo
         * 
         * @param String $usuario
         * @param String $password
         * 
         * @return boolean | array on successfully query
         */
        public function check_login($usuario, $password) {
                $sql = "SELECT * FROM Pueblo WHERE Usuario = :usuario AND password = :password";

                $this->database->connect();
                $result = $this->database->select($sql, array(
                        ":usuario" => $usuario,
                        ":password" => $password
                ));
                $this->database->close();

                return empty($result) ? false : $result;
        }
        

}
