<?php

/**
 * Mi Municipio al Dia
 * 
 * @author Marcos Lopez
 * 
 * Eventos model
 * 
 * Data operations with table Evento
 */
require_once 'core/model.php';
require_once 'inc/lugar.php';

class eventos_model extends Model {

        public function __construct() {
                parent::__construct();
                $this->table = "Evento";
        }

        /**
         * Validate form
         * 
         * @param type $datos
         * 
         * @return boolean
         */
        private function validate_form() {

                //check we have the needed data

                $needed = array(
                        "nombre_evento",
                        'descripcion_evento',
                        'tipo_evento',
                        'dia_evento',
                        'inicio_evento',
                        'fin_evento',
                        'lugar_evento'
                );

                foreach ($needed as $key => $value) {
                        if (!isset($_POST[$value]))
                                $r = new Response(400, null, 'Hay errores en los datos', 'Validation error. Lacking of neccesary fields');
                }

                if (empty($_POST["nombre_evento"])) {
                        $r = new Response(400, null, 'El nombre no puede estar vacío', 'Validation error. nombre_fiesta cannot be empty');
                }

                if (empty($_POST["lugar_evento"])) {
                        $r = new Response(400, null, 'El nombre no puede estar vacío', 'Validation error. nombre_fiesta cannot be empty');
                }

                if (empty($_POST["dia_evento"])) {
                        $r = new Response(400, null, 'La fecha no puede estar vacía', 'Validation error. dia_evento cannot be empty');
                }

                if (!preg_match("/\d{4}/", $_POST["inicio_evento"])) {
                        $r = new Response(400, null, 'La hora de inicio es errónea', 'Validation error. error with inicio_evento');
                }

                return true;
        }

        /**
         * @Funcionalidad: transforma los datos del formulario en un input valido y
         * adecuado para la base de datos
         * @return type
         */
        private function adapt_form($id_fiesta = null) {

                if (!($coords = Lugar::check_lugar($_POST["lugar_evento"]))) {
                        $coords = Lugar::get_coords($_POST["lugar_evento"], NOMBRE_PUEBLO, COMUNIDAD_AUTONOMA);

                        //Add to database

                        $sql = "INSERT INTO Lugar VALUES(:nombre, :lat, :long, :id_pueblo)";
                        
                        $datos_lugar = array(
                                ":nombre" => $_POST["lugar_evento"],
                                ":lat" => isset($coords['Latitud']) ? $coords['Latitud'] : $coords['lat'],
                                ":long" => isset($coords['Longitud']) ? $coords['long'] : $coords['long'],
                                ":id_pueblo" => ID_PUEBLO
                        );

                        $this->database->connect();
                        $this->database->no_select($sql, $datos_lugar);
                        $this->database->close();
                }

                $datos = array(
                        ":nombre" => $_POST['nombre_evento'],
                        ":descripcion" => $_POST["descripcion_evento"],
                        ":tipo" => (int) $_POST["tipo_evento"],
                        ":dia" => $this->format_in_date($_POST['dia_evento']),
                        ":hora_inicio" => $_POST["inicio_evento"],
                        ":hora_fin" => $_POST["fin_evento"],
                        ":lat" => isset($coords['Latitud']) ? $coords['Latitud'] : $coords['lat'],
                                ":long" => isset($coords['Longitud']) ? $coords['long'] : $coords['long'],
                );

                if (!is_null($id_fiesta)) {
                        $datos[":id_fiesta"] = (int) $id_fiesta;
                } else {
                        $r = new Response(400, null, 'Ausencia de identificación', 'Lacking id_fiesta');
                }

                return $datos;
        }

        /**
         * Call database to obtain all fiestas from a specific pueblo
         * 
         * @return $result
         */
        public function get_eventos($id_fiesta) {

                $this->database->connect();

                $sql = "SELECT idEvento, Nombre, Descripcion, Tipo, " .
                        "concat_ws(' ', DATE_FORMAT(Dia,'%d/%m/%Y')," .
                        "concat_ws(' -> ', concat_ws(':',SUBSTR(HoraInicio,1,2),SUBSTR(HoraInicio,-2)), concat_ws(':',SUBSTR(HoraFin,1,2),SUBSTR(HoraFin,-2)))) AS Dia, " .
                        "VotacionLocal, numVotosLocal, VotacionVisitante, numVotosVisitante," .
                        "(SELECT Nombre FROM Lugar l WHERE l.Latitud = e.Latitud AND l.Longitud = e.Longitud) AS Lugar " .
                        "FROM Evento e WHERE idFiesta = :id_fiesta";

                $data = [
                        ":id_fiesta" => $id_fiesta
                ];

                $result = $this->database->select($sql, $data);

                if (!empty($result)) {

                        if (!isset($result[0])) {
                                $result = array(
                                        $result
                                );
                        }
                }

                $this->database->close();

                return $result;
        }

        /**
         * Call database to obtain all eventos (votaciones screen)
         * 
         * @param String | int $id_fiesta
         * 
         * @return $result
         */
        public function get_eventos_votaciones($id_fiesta) {

                $this->database->connect();

                $sql = "SELECT idEvento, Nombre," .
                        "DATE_FORMAT(Dia,'%d/%m/%Y') AS Dia," .
                        "VotacionLocal, numVotosLocal, VotacionVisitante, numVotosVisitante " .
                        "FROM Evento e WHERE idFiesta = :id_fiesta";

                $data = [
                        ":id_fiesta" => $id_fiesta
                ];

                $result = $this->database->select($sql, $data);

                if (!empty($result)) {

                        if (!isset($result[0])) {
                                $result = array(
                                        $result
                                );
                        }
                }

                $this->database->close();

                return $result;
        }

        /**
         * Insert a fiesta in the database
         */
        public function insert_evento($id_fiesta) {

                $sql = "INSERT INTO Evento VALUES(NULL, :nombre, :descripcion, :tipo, :dia, " .
                        ":hora_inicio, :hora_fin, 0,0,0,0,true,:id_fiesta,:lat,:long);";

                if ($this->validate_form()) {

                        $datos = $this->adapt_form($id_fiesta);

                        $this->database->connect();
                        $this->database->no_select($sql, $datos);

                        $fiestas = $this->get_eventos($id_fiesta);

                        $this->database->close();

                        return $fiestas;
                }
        }

        /**
         * Update an evento in the database
         */
        public function edit_evento($id_fiesta, $id_evento) {

                $sql = "UPDATE Evento SET Nombre = :nombre, Descripcion = :descripcion, Tipo = :tipo, Dia = :dia, " .
                        "HoraInicio = :hora_inicio, HoraFin = :hora_fin, Latitud = :lat, Longitud = :long " .
                        "WHERE idEvento = :id_evento AND idFiesta = :id_fiesta;";

                if ($this->validate_form()) {

                        $datos = $this->adapt_form($id_fiesta);
                        $datos[":id_evento"] = (int) $id_evento;

                        $this->database->connect();
                        $this->database->no_select($sql, $datos);

                        $fiestas = $this->get_eventos($id_fiesta);

                        $this->database->close();

                        return $fiestas;
                }
        }

        /**
         * Delete a fiesta in the database
         * 
         * @param String $id_fiesta
         */
        public function delete_evento($id_fiesta, $id_evento) {
                $sql = "DELETE FROM Evento WHERE idFiesta = :id_fiesta AND idEvento = :id_evento";
                $datos = [
                        ":id_fiesta" => $id_fiesta,
                        "id_evento" => $id_evento
                ];

                $this->database->connect();
                $this->database->no_select($sql, $datos);
                $fiestas = $this->get_eventos($id_evento);
                $this->database->close();

                return $fiestas;
        }

}
