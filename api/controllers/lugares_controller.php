<?php

require_once 'core/controller.php';

class lugares_controller extends Controller {

        private $model_name = 'lugares_model';
        private $model;

        public function __construct() {

                require "/models/{$this->model_name}.php";

                $this->model = new $this->model_name();
        }

        public function get_lugares() {

                return $this->model->get_lugares();
        }

        public function add_lugar() {
                $lugar = $_POST["nombre_lugar"];
                $this->model->add_lugar($lugar);
        }
        
        public function delete_lugar($params){
                
                $coords = explode("x",$params[0]);
                $lat = $coords[0];
                $long = $coords[1];
                
                $this->model->delete_lugar($lat,$long);
        }

}
