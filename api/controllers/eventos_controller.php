<?php

require_once 'core/controller.php';

class eventos_controller extends Controller {

        private $model_name = 'eventos_model';
        private $model;

        public function __construct() {

                require "/models/{$this->model_name}.php";

                $this->model = new $this->model_name();
        }

        /**
         * Retrieve all eventos associated to the user credentials 
         * 
         * @return array prepared array to be loaded in dataTables
         */
        public function get_eventos($params) {
                return $this->model->get_eventos($params[0]);
        }
        
        /**
         * Retrieve eventos (votaciones screen)
         */
        public function get_eventos_votaciones($params){
               return $this->model->get_eventos_votaciones($params[0]); 
        }
        
        
        /**
         * Retrieve eventos (votaciones screen)
         */
        public function export_eventos($params){
               return $this->model->export_eventos($params[0]); 
        }
        
        /**
         * Insert a new fiesta into the database
         */
        public function insert_evento($params){
                $this->model->insert_evento($params[0]);
        }
        
        /**
         * Update an existing fiesta in teh database
         * 
         * @param array $params URI params
         */
        public function edit_evento($params){
                $this->model->edit_evento($params[0],$params[1]);
        }

        /**
         * Delete an existing fiesta in the database
         * 
         * @param array $params URI params
         */
        public function delete_evento($params) {
                $this->model->delete_evento($params[0], $params[1]);
        }

}
