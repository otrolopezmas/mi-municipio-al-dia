<?php

/**
 * Mi Municipio al Dia
 * 
 * @author Marcos Lopez
 * 
 * Fiestas controller
 */
require_once 'core/controller.php';

class fiestas_controller extends Controller {

        private $model_name = 'fiestas_model';
        private $model;

        public function __construct() {

                require "/models/{$this->model_name}.php";

                $this->model = new $this->model_name();
        }

        /**
         * Retrieve all fiestas associated to the user credentials 
         * 
         * @return array prepared array to be loaded in dataTables
         */
        public function get_fiestas() {
                return $this->model->get_fiestas();
        }
        
        /**
         * Retrieve all fiestas associated to the user credentials 
         * 
         * @return array prepared array to be loaded in dataTables
         */
        public function get_fiesta($params) {
                return $this->model->get_fiesta($params[0]);
        }
        
        /**
         * Retrieve all fiestas associated to the user credentials 
         * 
         * @return array prepared array to be loaded in dataTables
         */
        public function get_fiesta_nula() {
                return $this->model->get_fiesta_nula();
        }
        
        
        /**
         * Insert a new fiesta into the database
         */
        public function insert_fiesta(){
                $this->model->insert_fiesta();
        }
        
        /**
         * Update an existing fiesta in teh database
         * 
         * @param array $params URI params
         */
        public function edit_fiesta($params){
                $this->model->edit_fiesta($params[0]);
        }

        /**
         * Delete an existing fiesta in the database
         * 
         * @param array $params URI params
         */
        public function delete_fiesta($params) {
                $this->model->delete_fiesta($params[0]);
        }

}
