<?php

/**
 * Mi Municipio al Dia
 * 
 * @author: Marcos Lopez
 * 
 * Login controller
 */
class login_controller {

        private $model_name = 'login_model';
        private $model;

        /**
         * Initializes pertinent model
         */
        public function __construct() {
                
                require_once "/models/{$this->model_name}.php";

                $this->model = new $this->model_name();
        }

        /**
         * Retrieves Basic auth token for client storage
         * 
         */
        public function login() {
                
                $respuesta = new Response(200, array(
                        'token' => $_SERVER['REDIRECT_HTTP_AUTHORIZATION'],
                        "nombre_pueblo" => NOMBRE_PUEBLO
                ));
                
        }

        /**
         * Check auth credentials
         * 
         * @return array User information
         */
        public function auth() {

                $username = $_SERVER['PHP_AUTH_USER'];
                $password = $_SERVER['PHP_AUTH_PW'];

                $pueblo = $this->model->check_login($username, $password);

                return $pueblo;
        }

}

?>