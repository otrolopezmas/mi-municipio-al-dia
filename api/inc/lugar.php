<?php

/**
 * Mi Municipio al Día
 * 
 * @author Marcos Lopez
 * 
 * Lugar class
 * 
 * Support operations with information from Lugar table
 */
class Lugar {

        public static function check_lugar($lugar) {

                $database = new DB();

                $database->connect();

                $sql = "SELECT Latitud, Longitud FROM Lugar WHERE Nombre = :nombre AND Pueblo_idPueblo = :id_pueblo LIMIT 1";
                $datos = array(
                        ":nombre" => $lugar,
                        ":id_pueblo" => ID_PUEBLO
                );

                $result = $database->select($sql, $datos);

                $database->close();
                
                return count($result) ? $result : false;
        }

        /**
         * Retrieve Google Maps coordinates from a given address
         * 
         * @param String $calle street
         * @param String $pueblo city
         * @param String $comunidad (Spanish) State
         * @param String $country Country
         * 
         * @return array latitude and longitude
         */
        public static function get_coords($calle, $pueblo, $comunidad, $country = "España") {

                $direccion_google = "$calle, $pueblo, $comunidad, $country";
                $resultado = file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%s', urlencode($direccion_google)));
                $resultado = json_decode($resultado, TRUE);

                $lat = $resultado['results'][0]['geometry']['location']['lat'];
                $long = $resultado['results'][0]['geometry']['location']['lng'];

                return array(
                        'lat' => $lat,
                        'long' => $long
                );
        }

}
