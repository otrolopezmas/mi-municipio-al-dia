<?php

require_once 'core/response.php';

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $response = new Response(
                404, 'Error de autorización', 'Error 404, unauthorized attempt'
        );
}

require_once 'db/db.php';
require_once 'config/routes.php';
require_once 'config/database.php';
require_once 'core/dispatcher.php';
require_once 'core/request.php';
require_once 'controllers/login_controller.php';


$login_controller = new login_controller();
$pueblo = $login_controller->auth();

if (!$pueblo) {
        $response = new Response(401, null, 'Credenciales de autenticación erróneos', 'Unauthorized');
}

define('ID_PUEBLO', $pueblo['idPueblo']);
define('NOMBRE_PUEBLO', $pueblo['Nombre']);
define('COMUNIDAD_AUTONOMA', $pueblo['Comunidad_Autonoma']);

$dispatcher = new Dispatcher($router);

$path = str_replace('/mi_municipio_al_dia_rest/', "/", $_SERVER['REQUEST_URI']);
$request = new Request($_SERVER['REQUEST_METHOD'], $path);

$params = $dispatcher->handle($request);

//var_dump($params);

if ($params == false) {
//path not found in routes
        $response = new Response(
                404, null, 'Recurso no encontrado. Contacte con el administrador.', 'Error 404 resource not found'
        );
}

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
        //Make global array $_POST available for using
        parse_str(file_get_contents("php://input"), $_POST);
}

$controller_name = $params["controller"] . "_controller";
$action = $params['action'];

require_once "controllers/{$controller_name}.php";

$controller = new $controller_name();

if (isset($params['values']))
        $result = $controller->$action($params['values']);
else
        $result = $controller->$action();

$respuesta = new Response(200, $result);

?>