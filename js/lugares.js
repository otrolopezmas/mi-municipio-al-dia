/**
 * Mi Municipio al Día
 *
 * @author Marcos Lopez
 *
 *
 */

var $table_lugares;

var url = '../api'

$(document).ready(function () {

    $('#name_bar').text(localStorage.getItem('nombre_pueblo'));

    $table_lugares = $('#lugares_table').DataTable({
        "sAjaxSource": url + "/lugares/",
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
            oSettings.jqXHR = $.ajax({
                "dataType": 'json',
                "type": "GET",
                "url": sSource,
                "data": "",
                xhrFields: {
                    withCredentials: true
                },
                headers: {
                    'Authorization': localStorage.getItem('token')
                },
                "success": fnCallback,
                "error": function (req, status, error) {
                    var response = JSON.parse(req.responseText);
                    $.growl.error({
                        message: response.error.message
                    });
                    console.log(response.error.dev_message)
                }
            });
        },
        'paging': false,
        'filtering': true,
        'searching': true,
        'language': {
            'url': "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        },
        "columnDefs": [
            {
                "targets": [1],
                "visible": false
            },
            {
                "targets": [2],
                "searchable": false,
                "filterable": false
            },

        ],
        'columns': [
            {
                'data': 'Nombre'
            },
            {
                data: 'Coordenadas'
            },
            {
                data: '',
                render: function () {
                    return "<button class='btn btn-danger btn-delete'>Eliminar</button>"
                }
                    }
        ]

    });


    $("#new_lugar").on("click", function (e) {
        e.preventDefault();

        if (!$('#nombre_lugar').val().trim().length) {
            $.growl.error({
                message: 'El nombre del lugar no puede estar vacío'
            });
            return false;
        }

        $.ajax({
            'url': url + "/lugares/",
            'method': 'POST',
            'data': {
                'nombre_lugar': $('#nombre_lugar').val(),
            },
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            xhrFields: {
                withCredentials: true
            },
            // crossDomain: true,
            headers: {
                'Authorization': localStorage.getItem('token')
            },
            success: function () {

                $('#nombre_lugar').val("");

                $table_lugares.ajax.reload();
            },

            //},
            error: function (req, status, error) {
                var response = JSON.parse(req.responseText);
                $.growl.error({
                    message: response.error.message
                });
                console.log(response.error.dev_message)
            }
        })

    });

    /**
     * Delete a lugar row making an ajax call to the server
     *
     * reload table on success
     * show error on fail
     */
    $('#lugares_table').on("click", '.btn-delete', function () {

        var row = $(this).parents('tr')[0];
        var data = $table_lugares.row(row).data();

        $.ajax({
            url: url + "/lugares/" + data.Coordenadas,
            method: 'DELETE',
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            xhrFields: {
                withCredentials: true
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            },

            success: function () {
                $table_lugares.ajax.reload();
            },

            error: function (req, status, error) {
                var response = JSON.parse(req.responseText);
                $.growl.error({
                    message: response.error.message
                });
                console.log(response.error.dev_message)
            }
        });


        $('button.disconnect').on("click", function (e) {
            e.preventDefault();
            localStorage.removeItem('token');

            window.location.replace('../views/login.html');


        });

    })
})