var $table_eventos,
    $table_eventos,
    $modal_eventos,
    $editor_eventos,
    $editor_eventos_title,
    $submit_button_evento,
    $id_fiesta = location.href.split("?")[1],
    $lugares;

var url = '../api'

$(document).ready(function () {

    $('#name_bar').text(localStorage.getItem('nombre_pueblo'));

    $modal_eventos = $('#editor_evento_modal'),
        $editor_eventos = $('#editor_evento'),
        $editor_eventos_title = $('#editor_title_evento'),
        $submit_button_evento = $('#editor_evento_submit_btn');



    $.ajax({
        'url': url + "/fiestas/" + $id_fiesta,
        'method': 'GET',
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        xhrFields: {
            withCredentials: true
        },
        // crossDomain: true,
        headers: {
            'Authorization': localStorage.getItem('token')
        },
        success: function (data) {
            var data = JSON.parse(data).data;

            $("#nombre_programa").text(data[0].Nombre);

            var inicio = data[0].Inicio.split("-");

            var box = $("<button>", {
                text: 'Todos',
                class: "botones_dias"
            })

            box.appendTo($("#cuadros_dias"));

            for (var i = 0; i < parseInt(data[0].Diff); i++) {
                var box = $("<button>", {
                    text: (parseInt(inicio[0]) + i) + "/" + inicio[1],
                    class: "botones_dias"
                })

                box.appendTo($("#cuadros_dias"));
            }

            $("#cuadros_dias").on("click", "button", function () {

                if ($(this).text() === "Todos") {
                    $table_eventos.search("").draw();
                } else {
                    $table_eventos.search($(this).text()).draw();
                }


                $('.botones_dias').css('background', '#E3E3E3');
                $('.botones_dias').css('color', 'inherit');
                $(this).css("background-color", "rgb(56, 56, 56)").css("color", "white");

            })




            console.log(data[0].Inicio.replace(/[-]/g, "/"));

            $(".datepicker").datepicker({
                    format: 'dd/mm/yyyy',
                    startDate: '0',
                    language: 'es',
                    startDate: data[0].Inicio.replace(/[-]/g, "/"),
                    endDate: data[0].Fin.replace(/[-]/g, "/")

                })
                .on('changeDate', function () {
                    $(this).datepicker('hide');
                });
        },

        //},
        error: function (req, status, error) {
            var response = JSON.parse(req.responseText);
            $.growl.error({
                message: response.error.message
            });
            console.log(response.error.dev_message)
        }
    })

    function load_lugares() {
        $.ajax({
            'url': url + "/lugares/",
            'method': 'GET',
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            xhrFields: {
                withCredentials: true
            },
            // crossDomain: true,
            headers: {
                'Authorization': localStorage.getItem('token')
            },
            success: function (data) {

                var lugares = JSON.parse(data).data;
                var array = [];
                for (var i = 0, long = lugares.length; i < long; i++) {
                    array.push(lugares[i].Nombre)
                }

                var substringMatcher = function (strs) {
                    return function findMatches(q, cb) {
                        var matches, substringRegex;

                        // an array that will be populated with substring matches
                        matches = [];

                        // regex used to determine if a string contains the substring `q`
                        substrRegex = new RegExp(q, 'i');

                        // iterate through the pool of strings and for any string that
                        // contains the substring `q`, add it to the `matches` array
                        $.each(strs, function (i, str) {
                            if (substrRegex.test(str)) {
                                matches.push(str);
                            }
                        });

                        cb(matches);
                    };
                };

                $("#lugar_evento").typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                }, {
                    name: 'array',
                    source: substringMatcher(array)
                });
            }
        });
    }

    load_lugares();

    $table_eventos = $('#eventos_table').DataTable({
        "sAjaxSource": url + "/fiestas/" + $id_fiesta + /eventos/,
        "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
            oSettings.jqXHR = $.ajax({
                "dataType": 'json',
                "type": "GET",
                "url": sSource,
                "data": "",
                xhrFields: {
                    withCredentials: true
                },
                headers: {
                    'Authorization': localStorage.getItem('token')
                },
                "success": fnCallback,
                "error": function (req, status, error) {
                    var response = JSON.parse(req.responseText);
                    $.growl.error({
                        message: response.error.message
                    });
                    console.log(response.error.dev_message)
                }
            });
        },
        'paging': false,
        'filtering': true,
        'searching': true,
        "columnDefs": [
            {
                "targets": [0, 2, 6, 7, 8, 9],
                "visible": false
            },
            {
                "targets": [0, 6, 7, 8, 9, 10],
                "searchable": false,
                "filterable": false
            },

        ],
        'language': {
            'url': "//cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        },
        'columns': [
            {
                'data': 'idEvento'
            },
            {
                'data': 'Nombre'
            },
            {
                'data': 'Descripcion',
            },
            {
                'data': 'Tipo',
                render: function (data) {
                    switch (parseInt(data)) {
                    case 1:
                        return "Taurino";
                        break;
                    case 2:
                        return "Musical";
                        break;
                    case 3:
                        return "Competición";
                        break;
                    case 4:
                        return "Infantil";
                        break;
                    case 5:
                        return "Ferias";
                        break;
                    case 6:
                        return "Religioso";
                        break;
                    case 7:
                        return "Otros";
                        break;
                    }
                }
                            },
            {
                'data': 'Dia',
                            },

            {
                'data': 'Lugar',
                            },
            {
                'data': 'VotacionLocal',
                            },
            {
                'data': 'numVotosLocal',
                            },
            {
                'data': 'VotacionVisitante',
                            },
            {
                'data': 'numVotosVisitante',
                            },
            {
                data: '',
                render: function (data) {
                    return "<button class='btn btn-default btn-editar' >Editar</button>   <button class='btn btn-info btn-copiar'>Copiar</button>   <button class='btn btn-danger btn-delete'>Eliminar</button>  <button data-toggle='popover' class='btn btn-warning btn-votacion'>Votos</button>";
                }
            }
    ]

    });


    $("#new_evento").on("click", function (e) {
        e.preventDefault();

        $type = 0;
        $editor_eventos[0].reset();
        $('#editor_evento_title').text("Añadir nuevo evento");
        $submit_button_evento.text("Añadir nuevo evento");
        $("#lugar_evento").prop("disabled", false);
        $('#editor_evento_modal').modal();
    });

    //Copy event

    $('#eventos_table').on("click", '.btn-copiar', function () {
        var row = $(this).parents('tr')[0];
        var data = $table_eventos.row(row).data();

        $editor_eventos[0].reset();


        $("#lugar_evento").prop("disabled", false);

        $('#nombre_evento').val(data.Nombre);
        $('#tipo_evento').val(data.Tipo);
        $('#descripcion_evento').val(data.Descripcion);
        $('#dia_evento').val((data.Dia).split(" ")[0]);
        $('#hora_inicio').val((data.Dia).split(" ")[1].split(":")[0]);
        $('#minuto_inicio').val((data.Dia).split(" ")[1].split(":")[1]);
        $('#hora_fin').val((data.Dia).split(" ")[3].split(":")[0]);
        $('#minuto_fin').val((data.Dia).split(" ")[3].split(":")[1]);
        $('#lugar_evento').val(data.Lugar);

        $type = 0;
        $editor_eventos_title.text("Copiar evento");
        $submit_button_evento.text("Copiar evento");
        $('#editor_evento_modal').modal();
    })

    $('#eventos_table').on("click", '.btn-editar', function () {

        var row = $(this).parents('tr')[0];
        var data = $table_eventos.row(row).data();

        $("#lugar_evento").prop("disabled", false);

        $('#id_evento').val(data.idEvento);
        $('#nombre_evento').val(data.Nombre);
        $('#tipo_evento').val(data.Tipo);
        $('#descripcion_evento').val(data.Descripcion);
        $('#dia_evento').val((data.Dia).split(" ")[0]);
        $('#hora_inicio').val((data.Dia).split(" ")[1].split(":")[0]);
        $('#minuto_inicio').val((data.Dia).split(" ")[1].split(":")[1]);
        $('#hora_fin').val((data.Dia).split(" ")[3].split(":")[0]);
        $('#minuto_fin').val((data.Dia).split(" ")[3].split(":")[1]);
        $('#lugar_evento').val(data.Lugar);

        $type = 1;
        $editor_eventos_title.text("Editar evento");
        $submit_button_evento.text("Modificar evento");
        $('#editor_evento_modal').modal();
    })

    $('#eventos_table').on("click", '.btn-votacion', function () {

        var row = $(this).parents('tr')[0];
        var data = $table_eventos.row(row).data();

        $(this).popover({
            title: "Votos",
            content: "Votación Local: " + data.VotacionLocal + " (total de " + data.numVotosLocal + " )\n" +
                "Votación Visitante: " + data.VotacionVisitante + " (total de " + data.numVotosVisitante + " )\n",
            trigger: 'focus'
        }).popover("show");

    })

    /**
     * Form on submit call pertienent method
     * 
     */
    $editor_eventos.on("submit", function (e) {
        e.preventDefault();

        switch ($type) {
        case 0:
        case 2:
            add_evento();
            break;
        case 1:
            edit_evento();
            break;
        }

    })

    $("#sin_lugar").on("click", function () {
        if ($("#sin_lugar").prop("checked")) {

            $("#lugar_evento").val("Sin Lugar").prop("disabled", true);

        } else if (!$("#sin_lugar").prop("checked")) {

            $("#lugar_evento").val("").prop("disabled", false);
        }
    })

    /**
     * Add a new evento row
     * 
     * reload table on success
     * show error on fail
     */
    function add_evento() {


        if (!validate_eventos_form())
            return false;

        if ($("#lugar_evento").prop("disabled")) {
            $("#lugar_evento").prop("disabled", false);
            var lugar = $("#lugar_evento").val();
            $("#lugar_evento").prop("disabled", true);
        } else {
            var lugar = $("#lugar_evento").val();
        }


        $.ajax({
            'url': url + "/fiestas/" + $id_fiesta + "/eventos/",
            'method': 'POST',
            'data': {
                'nombre_evento': $('#nombre_evento').val(),
                'descripcion_evento': $('#descripcion_evento').val(),
                'tipo_evento': $('#tipo_evento').val(),
                'dia_evento': $('#dia_evento').val(),
                'inicio_evento': $('#hora_inicio').val() + $('#minuto_inicio').val(),
                'fin_evento': ($('#hora_fin').val() + $('#minuto_fin').val()) === "--" ? "" : $('#hora_fin').val() + $('#minuto_fin').val(),
                'lugar_evento': lugar
            },
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            xhrFields: {
                withCredentials: true
            },
            // crossDomain: true,
            headers: {
                'Authorization': localStorage.getItem('token')
            },
            success: function () {
                $('#editor_evento_modal').modal('hide');
                load_lugares();
                $table_eventos.ajax.reload();
            },

            //},
            error: function (req, status, error) {
                var response = JSON.parse(req.responseText);
                $.growl.error({
                    message: response.error.message
                });
                console.log(response.error.dev_message)
            }
        })

    }

    /**
     * Modify a evento row through ajax calling
     *
     * reload table on success
     * show error on fail
     */
    function edit_evento() {

        if (!validate_eventos_form())
            return false;

        var url_put = url + "/fiestas/" + $id_fiesta + "/eventos/" + $('#id_evento').val();

        $.ajax({
            'url': url_put,
            'method': 'PUT',
            'data': {
                'nombre_evento': $('#nombre_evento').val(),
                'descripcion_evento': $('#descripcion_evento').val(),
                'tipo_evento': $("#tipo_evento").val(),
                'dia_evento': $('#dia_evento').val(),
                'inicio_evento': $('#hora_inicio').val() + $('#minuto_inicio').val(),
                'fin_evento': ($('#hora_fin').val() + $('#minuto_fin').val()) === "--" ? "" : $('#hora_fin').val() + $('#minuto_fin').val(),
                'lugar_evento': $("#lugar_evento").val()
            },
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            xhrFields: {
                withCredentials: true
            },
            // crossDomain: true,
            headers: {
                'Authorization': localStorage.getItem('token')
            },
            success: function () {
                $('#editor_evento_modal').modal('hide');
                load_lugares();
                $table_eventos.ajax.reload();
            },

            //},
            error: function (req, status, error) {
                alert(error);
            }
        })

    }

    function get_tipo_number(tipo) {
        switch (tipo) {
        case "Taurino":
            return 1;
        case "Musical":
            return 2;
        case "Competición (deportes, torneos, etc)":
            return 3;
        case "Infantil":
            return 4;
        case "Ferias":
            return 5;
        case "Religioso":
            return 6;
        case "Otros":
            return 7;
        }
    }

    /**
     * Delete a evento row making an ajax call to the server
     *
     * reload table on success
     * show error on fail
     */
    $('#eventos_table').on("click", '.btn-delete', function () {

        var row = $(this).parents('tr')[0];
        var data = $table_eventos.row(row).data();

        var id_evento = data.idEvento;

        $.ajax({
            url: url + "/fiestas/" + $id_fiesta + "/eventos/" + id_evento,
            method: 'DELETE',
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            xhrFields: {
                withCredentials: true
            },

            headers: {
                'Authorization': localStorage.getItem('token')
            },

            success: function () {
                $('#editor_evento_modal').modal('hide');
                load_lugares();
                $table_eventos.ajax.reload();
            },

            error: function (req, status, error) {
                var response = JSON.parse(req.responseText);
                $.growl.error({
                    message: response.error.message
                });
                console.log(response.error.dev_message)
            }
        })


    });

    /**
     * Validate evento form
     * 
     * Conditions:
     *  nombre_evento cannot be empty
     *  fin_evento cannot be sooner than fin_evento
     * 
     * @returns boolean on success
     */
    function validate_eventos_form() {

        var error = true;

        if ($('#nombre_evento').val().length === 0) {
            $.growl.error({
                message: 'El nombre no puede estar vacío'
            });

            error = false;
        }

        if ($('#tipo_evento').val() === "-") {
            $.growl.error({
                message: 'El nombre no puede estar vacío'
            });

            error = false;
        }

        if ($('#dia_evento').val().length === 0) {
            $.growl.error({
                message: 'La fecha no puede estar vacía'
            });

            error = false;
        }

        if ($('#hora_inicio').val() === "-" || $('#minuto_inicio').val() === "-") {
            $.growl.error({
                message: 'La hora y el minuto de inicio deben estar definidos'
            });

            error = false;
        }

        if (($('#hora_fin').val() === "-" && $('#minuto_fin').val() !== "-") || ($('#hora_fin').val() !== "-" && $('#minuto_fin').val() == "-")) {
            $.growl.error({
                message: 'La hora de fin no está bien definida'
            });

            error = false;
        }

        if ($('#lugar_evento').val().length === 0) {
            $.growl.error({
                message: 'El lugar no puede estar vacío'
            });

            error = false;
        }

        return error;


    }

    $("#enviar_programa").on("click", function () {
        window.location = "fiestas.html";
    })

    $('button.disconnect').on("click", function (e) {
        e.preventDefault();
        localStorage.removeItem('token');

        window.location.replace('../views/login.html');


    });

})