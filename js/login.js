var url = '../api';

$(document).ready(function () {

    $('#login_form').on("submit", function (e) {

        e.preventDefault();

        if (!validacion_login())
            return false;

        var user = $('#form_user').val();
        var password = $('#form_pass').val();

        var auth = 'Basic ' +

            $.ajax({
                type: 'POST',
                url: url + '/login/',
                data: {
                    username: user,
                    password: password,
                },
                dataType: "json",
                contentType: 'application/x-www-form-urlencoded; charset=utf-8',
                xhrFields: {
                    withCredentials: true
                },
                // crossDomain: true,
                headers: {
                    'Authorization': 'Basic ' + btoa(user + ':' + password)
                },
                //beforeSend: function (xhr) {
                //},
                success: function (data) {
                    localStorage.setItem('token', data.data.token);
                    localStorage.setItem('nombre_pueblo', data.data.nombre_pueblo);
                    window.location.replace('../views/fiestas.html');
                },
                //complete: function (jqXHR, textStatus) {
                //},
                error: function (req, status, error) {
                    var response = JSON.parse(req.responseText);
                    $.growl.error({
                        message: response.error.message
                    });
                    console.log(response.error.dev_message)
                }
            });
    });

    function validacion_login() {

        var error = true;

        if ($('#form_user').val().length === 0) {
            $.growl.error({
                message: 'El campo de usuario no puede estar vacío'
            });

            error = false;
        }

        if ($('#form_pass').val().length === 0) {
            $.growl.error({
                message: 'El campo de contraseña no puede estar vacío'
            });

            error = false;
        }

        return error;
    }
});